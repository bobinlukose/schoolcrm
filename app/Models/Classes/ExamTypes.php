<?php

namespace App\Models\Classes;

use App\Support\HasAdvancedFilter;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DateTimeInterface;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;

class ExamTypes extends Model
{
    use HasAdvancedFilter, SoftDeletes, Notifiable, HasFactory;

    public $table = 'exam_types';

    protected $orderable = [
        'id',
        'title',
    ];

    protected $filterable = [
        'id',
        'title',
        'permissions.title',
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'title',
        'unique_id',
        'description',
        'brach_id',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }

    public function permissions()
    {
        return $this->belongsToMany(Permission::class);
    }
}
