<?php

namespace App\Models\Classes;

use App\Support\HasAdvancedFilter;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use DateTimeInterface;

class Sections extends Model
{
    use HasAdvancedFilter, SoftDeletes, Notifiable, HasFactory;

    public $table = 'sections';


    protected $orderable = [
        'id',
        'name',
    ];

    protected $filterable = [
        'id',
        'name',
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'name',
        'unique_id',
        'short_text',
        'branch_id',
        'class_id',
        'is_group',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function classes()
    {
        return $this->belongsTo(Classes::class,'class_id');
    }

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }

    public function permissions()
    {
        return $this->belongsToMany(Permission::class);
    }
}
