<?php

namespace App\Models\Classes;

use App\Models\Branches\Branch;
use App\Models\Exams\MarkTypes;
use App\Models\Exams\Terms;
use App\Support\HasAdvancedFilter;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DateTimeInterface;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;

class Exam extends Model
{

    use HasAdvancedFilter, SoftDeletes, Notifiable, HasFactory;

    public $table = 'exams';

    protected $orderable = [
        'id',
        'exam_title',
    ];

    protected $filterable = [
        'id',
        'title',
        'permissions.title',
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'exam_title',
        'unique_id',
        'branch_id',
        'start_date',
        'end_date',
        'type_id',
        'code',
        'class_id',
        'lesson',
        'section_id',
        'term',
        'mark_type_id',
        'status',
        'comments',
        'subject',
        'topic',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }

    public function permissions()
    {
        return $this->belongsToMany(Permission::class);
    }
    public function classesList(){
        return $this->belongsTo(Classes::class, 'class_id');
    }
    public function sectionType(){
        return $this->belongsTo(Sections::class, 'section_id');
    }
    public function markType(){
        return $this->belongsTo(MarkTypes::class, 'mark_type_id');
    }
    public function examType(){
        return $this->belongsTo(ExamTypes::class, 'type_id');
    }
    public function examSubject(){
        return $this->belongsTo(Subjects::class, 'subject_id');
    }
    public function examBranch(){
        return $this->belongsTo(Branch::class, 'branch_id');
    }

    public function examTerm(){
        return $this->belongsTo(Terms::class, 'term');
    }
}
