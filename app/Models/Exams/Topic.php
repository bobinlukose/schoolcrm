<?php

namespace App\Models\Exams;

use App\Models\Classes\Subjects;
use App\Support\HasAdvancedFilter;
use DateTimeInterface;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Topic extends Model
{
    use HasAdvancedFilter, SoftDeletes, HasFactory;

    public $table = 'topic';

    protected $casts = [
        'is_published' => 'boolean',
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $filterable = [
        'id',
        'subjects.name',
        'lesson.title',
        'title',
        'description',
    ];

    protected $orderable = [
        'id',
        'subjects.name',
        'lesson.title',
        'title',
        'description',
        'is_published',
    ];

    protected $fillable = [
        'subject_id',
        'lesson_id',
        'title',
        'description',
        'is_published',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }

    public function subjects()
    {
        return $this->belongsTo(Subjects::class,'subject_id');
    }

    public function lesson()
    {
        return $this->belongsTo(Lesson::class,'lesson_id');
    }
}
