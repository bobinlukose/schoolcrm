<?php

namespace App\Models\Exams;

use App\Support\HasAdvancedFilter;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use DateTimeInterface;

class MarkTypes extends Model
{
    use HasAdvancedFilter, SoftDeletes, Notifiable, HasFactory;

    public $table = 'mark_types';

    protected $casts = [
        'is_published' => 'boolean',
        'is_group' => 'boolean',
    ];


    protected $orderable = [
        'id',
        'title',
    ];

    protected $filterable = [
        'id',
        'title',
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'title',
        'unique_id',
        'branch_id',
        'entry_type',
        'entry_type',
        'maximum_mark',
        'passing_mark',
        'is_published',
        'order',
        'status',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }

    public function permissions()
    {
        return $this->belongsToMany(Permission::class);
    }
}
