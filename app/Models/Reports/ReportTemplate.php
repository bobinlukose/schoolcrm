<?php

namespace App\Models\Reports;

use App\Models\CrmStatus;
use App\Support\HasAdvancedFilter;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DateTimeInterface;

class ReportTemplate extends Model
{
    use HasAdvancedFilter, SoftDeletes, HasFactory;

    public $table = 'report_templates';

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $orderable = [
        'id',
        'template_name',
        'path',

    ];

    protected $filterable = [
        'id',
        'template_name',
        'path',
    ];

    protected $fillable = [
        'template_name',
        'path',
        'is_published',
        'class_id',
        'branch_id',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }

    public function status()
    {
        return $this->belongsTo(CrmStatus::class);
    }
}
