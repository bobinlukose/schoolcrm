<?php

namespace App\Models\Common;

use App\Support\HasAdvancedFilter;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use DateTimeInterface;

class Addresses extends Model
{
    use HasAdvancedFilter, SoftDeletes, Notifiable, HasFactory;

    public $table = 'addresses';




    protected $orderable = [
        'id',
        'type',
    ];

    protected $filterable = [
        'id',
        'ntypeame',
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'type',
        'parent_id',
        'street',
        'city',
        'state',
        'zip',
        'longitude',
        'latitude',
        'latitude',
        'branch_id',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }

    public function permissions()
    {
        return $this->belongsToMany(Permission::class);
    }
}
