<?php

namespace App\Repositories\Classes;

use App\Http\Resources\Admin\ExamResource;

use App\Models\Classes\Classes;
use App\Models\User;
use DB;

class ClassesRepository{

    private $dynamicModel,$multiSelectfilters;

    public function getAll(){
        return Classes::with(['branch']);
    }

    public function getAllClass(){
        return Classes::get(['id as class_id','name as class_name']);
    }

    public function getById($id){

    }

    public function save($data){

    }

    public function delete($id){

    }

}
