<?php

namespace App\Repositories\Classes;

use App\Http\Resources\Admin\ExamResource;

use App\Models\Classes\Classes;
use App\Models\Exams\Lesson;
use App\Models\User;
use DB;

class LessonRepository{

    private $dynamicModel,$multiSelectfilters;

    public function __construct(){

    }
    public function getAll(){
        return Lesson::with(['subjects']);
    }

    public function getAllPublishedLesson(){
        return Lesson::where('is_published','=',ACTIVE)->get(['id','title as lesson']);
    }

    public function getById($id){

    }

    public function save($data){

    }

    public function delete($id){

    }

}
