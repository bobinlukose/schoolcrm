<?php

namespace App\Repositories\Terms;

use App\Http\Resources\Admin\ExamResource;
use App\Models\Exams\Terms;
use App\Models\User;
use DB;

class TermsRepository{

    private $dynamicModel,$multiSelectfilters;

    public function __construct(){

    }
    public function getAll(){
        return new ExamResource(Terms::get(['id as term_id','name as exam_term']));
    }

    public function getById($id){
        return new ExamResource(Exam::with(['classesList','sectionType','markType','examType','examSubject','examBranch'])->advancedFilter()->find($id));
    }

    public function save($data){
        return Exam::insert($data);
    }

    public function delete($id){
        return Exam::destroy($id);
    }

}
