<?php

namespace App\Repositories\Exam;

use App\Http\Resources\Admin\ExamResource;

use App\Models\Classes\Exam;

use App\Models\User;
use DB;

class ExamRepository{

    private $dynamicModel,$multiSelectfilters;

    public function __construct(){

    }
    public function getAll(){
        return new ExamResource(Exam::with(['classesList','sectionType','markType','examType','examSubject','examBranch','examTerm'])->advancedFilter());
    }

    public function getById($id){
        return new ExamResource(Exam::with(['classesList','sectionType','markType','examType','examSubject','examBranch','examTerm'])->advancedFilter()->find($id));
    }

    public function save($data){
        return Exam::insert($data);
    }

    public function delete($id){
        return Exam::destroy($id);
    }

}
