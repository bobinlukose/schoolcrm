<?php

namespace App\Repositories\Exam;

use App\Http\Resources\Admin\ExamResource;

use App\Models\Classes\ExamTypes;
use App\Models\User;
use DB;

class ExamTypeRepository{

    private $dynamicModel,$multiSelectfilters;

    public function __construct(){

    }
    public function getAll(){
        return new ExamResource(ExamTypes::get(['id','title as exam_type']));
    }

    public function getById($id){

    }

    public function save($data){

    }

    public function delete($id){

    }

}
