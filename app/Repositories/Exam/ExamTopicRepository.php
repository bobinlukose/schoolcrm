<?php

namespace App\Repositories\Exam;

use App\Http\Resources\Admin\ExamResource;


use App\Models\Exams\Topic;

use DB;

class ExamTopicRepository{

    private $dynamicModel,$multiSelectfilters;

    public function __construct(){

    }
    public function getAll(){
        return Topic::with(['subjects', 'lesson']);
    }

    public function getAllPublishedTopic(){
        return Topic::where('is_published','=',ACTIVE)->get(['id','title as exam_topic']);
    }

    public function getById($id){

    }

    public function save($data){

    }

    public function delete($id){

    }

}
