<?php

namespace App\Repositories\Management;

use App\Http\Resources\Admin\ExamResource;
use App\Http\Resources\Admin\ManagementResource;

use App\Models\Classes\Exam;
use App\Models\Exams\MarkTypes;
use App\Models\Management\Management;
use DB;

class ManagementRepository{

    private $dynamicModel,$multiSelectfilters;

    public function __construct(){

    }
    public function getAll(){
        return  Management::join('addresses', 'management.id', '=', 'addresses.parent_id')
            ->select('management.*', 'addresses.street', 'addresses.city','addresses.zip','addresses.state')
            ->where('addresses.type', '=', MANAGEMENT_ADDRESS);

    }

    public function getById($id){

    }

    public function create($request){
        return Management::create($request->all());
    }

    public function delete($id){

    }

}
