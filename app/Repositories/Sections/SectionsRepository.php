<?php

namespace App\Repositories\Sections;

use App\Http\Resources\Admin\ExamResource;
use App\Models\Classes\Sections;
use App\Models\User;
use DB;

class SectionsRepository{

    private $dynamicModel,$multiSelectfilters;

    public function __construct(){

    }

    public function getAll(){
        return Sections::with(['classes']);

    }

    public function getAllSection(){
        return new ExamResource(Sections::get(['id as section_id','name as section_name']));
    }

    public function getById($id){

    }

    public function save($data){
        return Sections::create($data);
    }

    public function delete($id){
        return Sections::destroy($id);
    }

}
