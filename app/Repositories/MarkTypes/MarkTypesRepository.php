<?php

namespace App\Repositories\MarkTypes;

use App\Http\Resources\Admin\ExamResource;


use App\Models\Exams\MarkTypes;
use App\Models\User;
use DB;

class MarkTypesRepository{

    private $dynamicModel,$multiSelectfilters;

    public function __construct(){

    }
    public function getAll(){
        return MarkTypes::whereIn('is_published', [ACTIVE,INACTIVE]);
    }

    public function getAllPublishedMarkType(){
        return MarkTypes::where('is_published','=',ACTIVE)->get(['id as mark_type_id','title as mark_type_name']);
    }

    public function save($data){
        return MarkTypes::create($data);
    }

    public function delete($id){
        return MarkTypes::destroy($id);
    }

}
