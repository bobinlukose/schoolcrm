<?php

namespace App\Repositories\Students;

use App\Http\Resources\Admin\ExamResource;
use App\Http\Resources\Admin\ManagementResource;

use App\Models\Classes\Exam;
use App\Models\Classes\Students;
use App\Models\Exams\MarkTypes;
use App\Models\Management\Management;
use App\Models\Students\StudentsModel;
use DB;

class StudentsRepository{

    private $dynamicModel,$multiSelectfilters;

    public function __construct(){

    }
    public function getAll(){
          StudentsModel::join('addresses', 'students.id', '=', 'addresses.parent_id')
            ->select('students.*', 'addresses.street', 'addresses.city','addresses.zip','addresses.state')
            ->where('addresses.type', '=', STUDENT_ADDRESS);
        return StudentsModel::with(['classes','branch']);

    }

    public function getById($id){

    }

    public function create($request){
        return StudentsModel::create($request->all());
    }

    public function delete($id){

    }

}
