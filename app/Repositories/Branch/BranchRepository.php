<?php

namespace App\Repositories\Branch;

use App\Http\Resources\Admin\ExamResource;

use App\Models\Branches\Branch;
use App\Models\User;
use DB;

class BranchRepository{

    private $dynamicModel,$multiSelectfilters;

    public function __construct(){

    }
    public function getAll(){
       return Branch::with(['management']);
    }

    public function getAllBranches(){
        return Branch::get(['id as branch_id','name as branch_name']);
    }

    public function getById($id){

    }

    public function save($data){

    }

    public function delete($id){

    }

}
