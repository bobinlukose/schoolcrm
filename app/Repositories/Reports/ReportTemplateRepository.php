<?php

namespace App\Repositories\Reports;

use App\Http\Resources\Admin\ExamResource;


use App\Models\Reports\ReportTemplate;
use App\Models\User;
use DB;

class ReportTemplateRepository{

    private $dynamicModel,$multiSelectfilters;

    public function __construct(){
        return ReportTemplate::whereIn('is_published', [ACTIVE,INACTIVE]);
    }
    public function getAll(){

    }
    public function save($data){
       return ReportTemplate::create($data);
    }

    public function update($data){
       return ReportTemplate::update($data);
    }

    public function delete($id){
        return ReportTemplate::destroy($id);
    }

}
