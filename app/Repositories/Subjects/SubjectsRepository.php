<?php

namespace App\Repositories\Subjects;

use App\Http\Resources\Admin\ExamResource;

use App\Http\Resources\Admin\SubjectsResource;
use App\Models\Classes\Subjects;
use App\Models\User;
use DB;

class SubjectsRepository{

    private $dynamicModel,$multiSelectfilters;

    public function __construct(){

    }
    public function getAll(){
        return Subjects::whereIn('is_published', [ACTIVE,INACTIVE]);
    }

    public function getAllPublishedSubjects(){
        return Subjects::where('is_published','=',ACTIVE)->get(['id as subject_id','name as subject_name']);
    }

    public function getById($id){
        return Subjects::find($id);
    }

    public function save($data){
        return Subjects::create($data);
    }

    public function delete($id){
        return Subjects::destroy($id);
    }

}
