<?php

namespace App\Http\Controllers\Api\V1\Admin\School;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreMarkTypeRequest;
use App\Http\Requests\StoreSubjectsRequest;
use App\Http\Requests\UpdateMarkTypeRequest;
use App\Http\Resources\Admin\MarkTypeResource;
use App\Models\Exams\MarkTypes;
use App\Services\Classes\MarkTypeService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Gate;

class MarkTypeApiController extends Controller
{
    private $markTypeService;


    public function __construct(MarkTypeService $markTypeService) {
        $this->markTypeService = $markTypeService;
    }

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        abort_if(Gate::denies('mark_type_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new MarkTypeResource($this->markTypeService->getAll()->advancedFilter());
    }


    public function create()
    {
        abort_if(Gate::denies('mark_type_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return response([
            'meta' => [],
        ]);

    }

    public function store(StoreMarkTypeRequest$request)
    {
        $markType = $this->markTypeService->create($request->validated());

        return (new MarkTypeResource($markType))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }


    public function edit(MarkTypes $MarkType)
    {
        abort_if(Gate::denies('mark_type_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return response([
            'data' => new MarkTypeResource($MarkType),
            'meta' => [],
        ]);
    }

    public function update(UpdateMarkTypeRequest $request, MarkTypes $markType)
    {
        $markType->update($request->validated());

        return (new MarkTypeResource($markType))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);
    }

    public function destroy(MarkTypes $MarkType)
    {
        abort_if(Gate::denies('mark_type_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $MarkType->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
