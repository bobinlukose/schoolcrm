<?php

namespace App\Http\Controllers\Api\V1\Admin\Management;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreManagementRequest;
use App\Http\Requests\UpdateManagementRequest;
use App\Http\Resources\Admin\ManagementResource;
use App\Models\Management\Management;
use App\Services\ManagementService;
use App\Traits\FileUploader;
use Gate;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class ManagementApiController extends Controller
{
    use FileUploader;

    private $managementService;

    public function __construct() {
        $this->managementService = new ManagementService();
    }

    public function index()
    {
        abort_if(Gate::denies('management_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return  new ManagementResource($this->managementService->getAll()->jsonPaginate());
    }

    public function store(StoreManagementRequest $request)
    {
        $management = $this->managementService->create($request);
        if ($media = $request->input('thumbnail', [])) {
            Media::whereIn('id', data_get($media, '*.id'))
                ->where('model_id', 0)
                ->update(['model_id' => $management->id]);
        }

        return (new ManagementResource($management))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    public function create()
    {
        abort_if(Gate::denies('management_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return response([
            'meta' => [],
        ]);
    }

    public function show(Management $management)
    {
        abort_if(Gate::denies('management_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new ManagementResource($management);
    }

    public function update(UpdateManagementRequest $request, Management $management)
    {
        $management->update($request->validated());

        return (new ManagementResource($management))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);
    }

    public function edit(Management $management)
    {
        abort_if(Gate::denies('management_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return response([
            'data' => new ManagementResource($management),
            'meta' => [],
        ]);
    }

    public function destroy(Management $management)
    {
        abort_if(Gate::denies('management_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $management->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }

    public function storeMedia(Request $request)
    {
       abort_if(Gate::none(['management_create', 'management_edit']), Response::HTTP_FORBIDDEN, '403 Forbidden');
       $media =  $this->storeTraitMedia($request,new Management());
       return response()->json($media, Response::HTTP_CREATED);
    }

}
