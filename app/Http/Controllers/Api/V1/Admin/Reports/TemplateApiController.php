<?php

namespace App\Http\Controllers\Api\V1\Admin\Reports;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreReportTemplateRequest;
use App\Http\Requests\UpdateReportTemplateRequest;
use App\Http\Resources\Admin\ReportTemplateResource;
use App\Models\Branches\Branch;
use App\Models\Classes\Classes;
use App\Models\Classes\Subjects;
use App\Models\Reports\ReportTemplate;
use App\Models\Students\StudentsModel;
use App\Services\Reports\ReportTemplateService;
use Illuminate\Http\Request;
use Gate;
use Illuminate\Http\Response;

class TemplateApiController extends Controller
{
    private $reportTemplateService;


    public function __construct(ReportTemplateService $reportTemplateService) {
        $this->reportTemplateService = $reportTemplateService;
    }


    public function index()
    {
        abort_if(Gate::denies('report_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new ReportTemplateResource(ReportTemplate::with(['status'])->advancedFilter());
    }

    public function store(StoreReportTemplateRequest $request)
    {
        $ReportTemplate = $this->reportTemplateService->create($request->validated());
        return (new ReportTemplateResource($ReportTemplate))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    public function create()
    {
        abort_if(Gate::denies('report_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return response([
            'meta' => [
                'status' => CrmStatus::get(['id', 'name']),
            ],
        ]);
    }

    public function show(ReportTemplate $report)
    {
        abort_if(Gate::denies('report_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new ReportTemplateResource($report);
    }

    public function update(UpdateReportTemplateRequest $request, ReportTemplate $report)
    {
        dd($request->all());
        $report->update($request->validated());
        $this->reportTemplateService->generateStudentReport($request->all());
        return (new ReportTemplateResource($report))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);
    }

    public function edit(ReportTemplate $template)
    {
        abort_if(Gate::denies('report_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $grades[0] = ['id'=>'1','grade'=>'A','max_marks'=>100,'minimum_marks'=>80];
        $grades[1] = ['id'=>'2','grade'=>'B','max_marks'=>79,'minimum_marks'=>65];
        $grades[2] = ['id'=>'3','grade'=>'C','max_marks'=>64,'minimum_marks'=>50];
        $grades[3] = ['id'=>'4','grade'=>'D','max_marks'=>49,'minimum_marks'=>35];
        $grades[4] = ['id'=>'5','grade'=>'E','max_marks'=>34,'minimum_marks'=>0];

        return response([
            'data' => new ReportTemplateResource($template),
            'meta' => [
              //  'subjects' => $this->reportTemplateService->getDataForReport(),
                'schoolSubjects' => Subjects::get(['id', 'name']),
                'branch' => Branch::get(['id', 'name']),
                'grades' => $grades,
                'classes' => Classes::get(['id', 'name']),
                'students' => StudentsModel::get(['id', 'first_name']),
            ],
        ]);
    }

    public function destroy(ReportTemplate $report)
    {
        abort_if(Gate::denies('report_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $report->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }

    public function generateReport(Request $request)
    {

       $this->reportTemplateService->generatePDF();
        return (1)
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);
    }

}
