<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Resources\Admin\BranchManagementResource;
use App\Http\Resources\Admin\ExamResource;
use App\Http\Resources\Admin\UserResource;
use App\Models\Branch;
use App\Models\Classes;
use App\Models\ExamTypes;
use App\Models\MarkTypes;
use App\Models\ProductCategory;
use App\Models\ProductTag;
use App\Models\Role;
use App\Models\Sections;
use App\Models\Subjects;
use App\Models\Terms;
use App\Models\User;
use App\Repositories\Exam\ExamRepository;
use App\Services\ExamService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Gate;

class ExamApiController extends Controller
{
    private $examService;


    public function __construct(ExamService $examService) {
        $this->examService = $examService;
    }

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        abort_if(Gate::denies('exam_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        return $this->examService->getAll();
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        abort_if(Gate::denies('user_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $masterData =  $this->examService->getMasterData();
        return response([
            'meta' => [
                'roles' => Role::get(['id', 'title']),
                'masterData' =>$masterData,
            ],
        ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $examData = $this->examService->saveExamData($request->all());
        $examData = [];
        return (new ExamResource($examData))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(int $id)
    {
        abort_if(Gate::denies('exam_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $masterData =  $this->examService->getMasterData();
        return response([
            'data' => $this->examService->getById($id),
            'meta' => [
                'roles' => Role::get(['id', 'title']),
                'masterData' =>$masterData,
            ],
        ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(int $id)
    {

        abort_if(Gate::denies('exam_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $this->examService->deleteById($id);

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
