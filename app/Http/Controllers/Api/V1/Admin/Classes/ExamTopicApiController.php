<?php

namespace App\Http\Controllers\Api\V1\Admin\Classes;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreExamTopicRequest;
use App\Http\Requests\UpdateExamTopicRequest;
use App\Http\Resources\Admin\ExamTopicResource;
use App\Models\Classes\Course;
use App\Models\Classes\Subjects;
use App\Models\Exams\Lesson;
use App\Models\Exams\Topic;
use App\Services\Classes\ExamTopicService;
use App\Services\Classes\LessonService;
use Gate;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class ExamTopicApiController extends Controller
{

    private $examTopicService;


    public function __construct(ExamTopicService $examTopicService) {
        $this->examTopicService = $examTopicService;
    }

    public function index()
    {
        abort_if(Gate::denies('test_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new ExamTopicResource($this->examTopicService->getAll()->advancedFilter());
    }

    public function store(StoreExamTopicRequest $request)
    {

        $test = Topic::create($request->validated());

        return (new ExamTopicResource($test))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    public function create()
    {
        abort_if(Gate::denies('test_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return response([
            'meta' => [
                'course' => Subjects::get(['id', 'name as title']),
                'lesson' => Lesson::get(['id', 'title']),
            ],
        ]);
    }

    public function show(Topic $test)
    {
        abort_if(Gate::denies('test_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new ExamTopicResource($test->load(['subjects', 'lesson']));
    }

    public function update(UpdateExamTopicRequest $request, Topic $test)
    {
        $test->update($request->validated());

        return (new ExamTopicResource($test))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);
    }

    public function edit(Topic $test)
    {
        abort_if(Gate::denies('test_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return response([
            'data' => new ExamTopicResource($test->load(['subjects', 'lesson'])),
            'meta' => [
                'course' => Subjects::get(['id', 'name as title']),
                'lesson' => Lesson::get(['id', 'title']),
            ],
        ]);
    }

    public function destroy(Topic $test)
    {
        abort_if(Gate::denies('test_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $test->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
