<?php

namespace App\Http\Controllers\Api\V1\Admin\Classes;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreSubjectsRequest;
use App\Http\Requests\UpdateSubjectsRequest;
use App\Http\Resources\Admin\SubjectsResource;
use App\Models\Classes\Subjects;
use App\Services\Classes\SubjectsService;
use Gate;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class SubjectsApiController extends Controller
{

    private $subjectService;


    public function __construct(SubjectsService $subjectService) {
        $this->subjectService = $subjectService;
    }

    public function index()
    {
        abort_if(Gate::denies('subject_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new SubjectsResource($this->subjectService->getAll()->advancedFilter());
    }

    public function store(StoreSubjectsRequest $request)
    {

        $subject = $this->subjectService->create($request->validated());

        return (new SubjectsResource($subject))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    public function create()
    {
        abort_if(Gate::denies('subject_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return response([
            'meta' => [],
        ]);
    }

    public function show(Subjects $subject)
    {
        abort_if(Gate::denies('subject_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new SubjectsResource($subject);
    }

    public function update(UpdateSubjectsRequest $request, Subjects $subject)
    {
        $subject->update($request->validated());

        return (new SubjectsResource($subject))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);
    }

    public function edit(Subjects $subject)
    {
        abort_if(Gate::denies('subject_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return response([
            'data' => new SubjectsResource($subject),
            'meta' => [],
        ]);
    }

    public function destroy(Subjects $subject)
    {
        abort_if(Gate::denies('subject_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $subject->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
