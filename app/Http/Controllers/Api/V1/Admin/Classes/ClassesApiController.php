<?php

namespace App\Http\Controllers\Api\V1\Admin\Classes;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreClassRequest;
use App\Http\Requests\UpdateClassRequest;
use App\Http\Resources\Admin\ClassResource;
use App\Models\Branches\Branch;
use App\Models\Classes\Classes;
use App\Models\Classes\Subjects;
use App\Services\Classes\ClassService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Gate;

class ClassesApiController extends Controller
{
    private $classService;


    public function __construct(ClassService $classService) {
        $this->classService = $classService;
    }

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        abort_if(Gate::denies('classes_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new ClassResource($this->classService->getAll()->advancedFilter());
    }

    /**
     * Show the form for creating a new resource.
     */

    public function create()
    {
        abort_if(Gate::denies('classes_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return response([
            'meta' => [
                'branch' => Branch::get(['id', 'name']),
            ],
        ]);
    }

    public function store(StoreClassRequest $request)
    {

        $class = $this->classService->create($request->validated());

        return (new ClassResource($class))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }


    public function show(Classes $class)
    {
        abort_if(Gate::denies('classes_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new ClassResource($class->load(['branch']));
    }



    public function edit(Classes $class)
    {
        abort_if(Gate::denies('classes_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return response([
            'data' => new ClassResource($class),
            'meta' => [
                'branch' => Branch::get(['id', 'name']),
            ],

        ]);
    }


    public function update(UpdateClassRequest $request, Classes $class)
    {
        $class->update($request->validated());

        return (new ClassResource($class))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);
    }


    public function destroy(Classes $class)
    {
        abort_if(Gate::denies('classes_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $class->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
