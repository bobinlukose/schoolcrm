<?php

namespace App\Http\Controllers\Api\V1\Admin\Classes;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreSectionRequest;
use App\Http\Requests\UpdateSectionRequest;
use App\Http\Resources\Admin\SectionResource;
use App\Models\Classes\Classes;
use App\Models\Classes\Sections;
use App\Services\Classes\LessonService;
use App\Services\Classes\SectionService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Gate;

class SectionApiController extends Controller
{
    private $sectionService;


    public function __construct(SectionService $sectionService) {
        $this->sectionService = $sectionService;
    }
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        abort_if(Gate::denies('lesson_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new SectionResource($this->sectionService->getAll()->advancedFilter());
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        abort_if(Gate::denies('section_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return response([
            'meta' => [
                'classes' => Classes::get(['id', 'name']),
            ],
        ]);
    }

    public function store(StoreSectionRequest $request)
    {
        $class = $this->sectionService->create($request->validated());
        if ($media = $request->input('thumbnail', [])) {
            Media::whereIn('id', data_get($media, '*.id'))
                ->where('model_id', 0)
                ->update(['model_id' => $class->id]);
        }

        if ($media = $request->input('video', [])) {
            Media::whereIn('id', data_get($media, '*.id'))
                ->where('model_id', 0)
                ->update(['model_id' => $class->id]);
        }

        return (new SectionResource($class))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    public function edit(Sections $section)
    {
        abort_if(Gate::denies('section_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return response([
            'data' => new SectionResource($section->load(['classes'])),
            'meta' => [
                'classes' => Classes::get(['id', 'name']),
            ],
        ]);
    }


    public function update(UpdateSectionRequest $request, Sections $section)
    {
        $section->update($request->validated());

        return (new SectionResource($section))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);
    }


    public function destroy(Sections $section)
    {
        abort_if(Gate::denies('section_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $section->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
