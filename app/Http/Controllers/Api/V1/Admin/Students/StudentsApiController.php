<?php

namespace App\Http\Controllers\Api\V1\Admin\Students;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreStudentsRequest;
use App\Http\Requests\UpdateStudentRequest;
use App\Http\Requests\UpdateStudentsRequest;
use App\Http\Resources\Admin\StudentResource;
use App\Models\Branches\Branch;
use App\Models\Classes\Classes;
use App\Models\Management\Management;
use App\Models\Students\StudentsModel;
use App\Services\Students\StudentService;
use App\Traits\FileUploader;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Gate;

class StudentsApiController extends Controller
{
    use FileUploader;

    private $studentService;

    public function __construct() {
        $this->studentService = new StudentService();
    }

    public function index()
    {
        abort_if(Gate::denies('students_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return  new StudentResource($this->studentService->getAll()->jsonPaginate());
    }

    public function store(StoreStudentsRequest $request)
    {
        $student = $this->studentService->create($request);
        if ($media = $request->input('thumbnail', [])) {
            Media::whereIn('id', data_get($media, '*.id'))
                ->where('model_id', 0)
                ->update(['model_id' => $student->id]);
        }

        return (new StudentResource($student))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    public function create()
    {
        abort_if(Gate::denies('students_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return response([
            'meta' => [
                'branch' => Branch::get(['id', 'name']),
                'classes' => Branch::get(['id', 'name']),
            ],
        ]);
    }

    public function show(StudentsModel $student)
    {
        abort_if(Gate::denies('students_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new StudentResource($student);
    }

    public function update(UpdateStudentsRequest $request, StudentsModel $student)
    {
        $student->update($request->validated());

        return (new StudentResource($student))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);
    }

    public function edit(StudentsModel $student)
    {
        abort_if(Gate::denies('students_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return response([
            'data' => new StudentResource($student),
            'meta' => [
                'branch' => Branch::get(['id', 'name']),
                'classes' => Classes::get(['id', 'name']),
            ],
        ]);
    }

    public function destroy(StudentsModel $student)
    {
        abort_if(Gate::denies('students_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $student->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }

    public function storeMedia(Request $request)
    {
        abort_if(Gate::none(['students_create', 'students_edit']), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $media =  $this->storeTraitMedia($request,new StudentsModel());
        return response()->json($media, Response::HTTP_CREATED);
    }

}
