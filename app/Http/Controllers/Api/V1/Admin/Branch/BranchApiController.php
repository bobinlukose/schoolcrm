<?php

namespace App\Http\Controllers\Api\V1\Admin\Branch;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreBranchResourceRequest;
use App\Http\Requests\UpdateBranchRequest;
use App\Http\Resources\Admin\BranchManagementResource;
use App\Models\Branches\Branch;
use App\Models\ContactCompany;
use App\Models\ContactContact;
use App\Models\Management\Management;
use App\Services\Branch\BranchService;
use Gate;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class BranchApiController extends Controller
{
    private $branchService;

    public function __construct() {
        $this->branchService = new BranchService();
    }

    public function index()
    {
        abort_if(Gate::denies('branch_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        return  new BranchManagementResource($this->branchService->getAll()->advancedFilter());
    }

    public function store(StoreBranchResourceRequest $request)
    {
        $branchDetail = Branch::create($request->validated());

        return (new BranchManagementResource($branchDetail))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    public function create()
    {
        abort_if(Gate::denies('branch_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return response([
            'meta' => [
                'management' => Management::get(['id', 'name']),
            ],
        ]);
    }

    public function show(Branch $branchDetail)
    {
        abort_if(Gate::denies('branch_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new BranchManagementResource($branchDetail->load(['management']));
    }

    public function update(UpdateBranchRequest $request, ContactContact $branchDetail)
    {
        $branchDetail->update($request->validated());

        return (new BranchManagementResource($branchDetail))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);
    }

    public function edit(Branch $branchDetail)
    {
        abort_if(Gate::denies('branch_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return response([
            'data' => new BranchManagementResource($branchDetail->load(['management'])),
            'meta' => [
                'management' => Management::get(['id', 'name']),
            ],
        ]);
    }

    public function destroy(Branch $branchDetail)
    {
        abort_if(Gate::denies('branch_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $branchDetail->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
