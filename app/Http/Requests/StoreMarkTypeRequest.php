<?php

namespace App\Http\Requests;

use App\Models\ExpenseCategory;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class StoreMarkTypeRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('mark_type_create');
    }

    public function rules()
    {
        return [
            'title' => [
                'string',
                'required',
            ],
            'maximum_mark' => [
                'integer',
                'required',
            ],
            'passing_mark' => [
                'integer',
                'required',
            ],
            'is_published' => [
                'boolean',
            ],
        ];
    }
}
