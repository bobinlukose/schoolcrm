<?php

namespace App\Http\Requests;

use App\Models\ExpenseCategory;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class StoreClassRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('classes_create');
    }

    public function rules()
    {
        return [
            'branch_id' => [
                'integer',
                'exists:branches,id',
                'required',
            ],
            'name' => [
                'string',
                'required',
            ],
        ];
    }
}
