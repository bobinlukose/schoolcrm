<?php

namespace App\Http\Requests;

use App\Models\Lesson;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class UpdateSectionRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('section_create');
    }

    public function rules()
    {
        return [
            'class_id' => [
                'integer',
                'exists:classes,id',
                'required',
            ],
            'name' => [
                'string',
                'required',
            ],
            'short_text' => [
                'string',
                'nullable',
            ],
        ];
    }
}
