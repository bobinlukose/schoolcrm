<?php

namespace App\Http\Requests;

use App\Models\ExpenseCategory;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class StoreSubjectsRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('subjects_create');
    }

    public function rules()
    {
        return [
            'name' => [
                'string',
                'required',
            ],
            'is_published' => [
                'boolean',
            ],
            'is_group' => [
                'boolean',
            ],
        ];
    }
}
