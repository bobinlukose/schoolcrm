<?php

namespace App\Services;

use App\Repositories\Branch\BranchRepository;
use App\Repositories\Classes\ClassesRepository;
use App\Repositories\Classes\LessonRepository;
use App\Repositories\Exam\ExamRepository;
use App\Repositories\Exam\ExamTopicRepository;
use App\Repositories\Exam\ExamTypeRepository;
use App\Repositories\MarkTypes\MarkTypesRepository;
use App\Repositories\Sections\SectionsRepository;
use App\Repositories\Subjects\SubjectsRepository;
use App\Repositories\Terms\TermsRepository;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use InvalidArgumentException;

class ExamService
{
    /**
     * @var $examRepository
     */
    protected $examRepository,$branchRepo,$classRepo,$examTypeRepo,$markTypeRepo,$sectionRepo,$subjectsRepo,$termRepo,$lessonRepo,$examTopicRepo;

    /**
     * PostService constructor.
     *
     * @param ExamRepository $examRepository
     */
    public function __construct(ExamRepository $examRepository)
    {
        $this->examRepository = $examRepository;
        $this->branchRepo = new BranchRepository();
        $this->classRepo = new ClassesRepository();
        $this->examTypeRepo = new ExamTypeRepository();
        $this->markTypeRepo = new MarkTypesRepository();
        $this->sectionRepo = new SectionsRepository();
        $this->subjectsRepo = new SubjectsRepository();
        $this->termRepo = new TermsRepository();
        $this->lessonRepo = new LessonRepository();
        $this->examTopicRepo = new ExamTopicRepository();
    }

    /**
     * Delete post by id.
     *
     * @param $id
     * @return String
     */
    public function deleteById($id)
    {
        DB::beginTransaction();

        try {
            $post = $this->examRepository->delete($id);

        } catch (Exception $e) {
            DB::rollBack();
            Log::info($e->getMessage());

            throw new InvalidArgumentException('Unable to ');
        }

        DB::commit();

        return $post;

    }

    /**
     * Get all post.
     *
     * @return String
     */
    public function getAll()
    {
        return $this->examRepository->getAll();
    }

    /**
     * Get post by id.
     *
     * @param $id
     * @return String
     */
    public function getById($id)
    {
        return $this->examRepository->getById($id);
    }

    /**
     * Update post data
     * Store to DB if there are no errors.
     *
     * @param array $data
     * @return String
     */
    public function updatePost($data, $id)
    {
        $validator = Validator::make($data, [
            'title' => 'bail|min:2',
            'description' => 'bail|max:255'
        ]);

        if ($validator->fails()) {
            throw new InvalidArgumentException($validator->errors()->first());
        }

        DB::beginTransaction();

        try {
            $post = $this->examRepository->update($data, $id);

        } catch (Exception $e) {
            DB::rollBack();
            Log::info($e->getMessage());

            throw new InvalidArgumentException('Unable to update post data');
        }

        DB::commit();

        return $post;

    }

    /**
     * Validate post data.
     * Store to DB if there are no errors.
     *
     * @param array $data
     * @return String
     */
    public function saveExamData($data)
    {
        $validator = Validator::make($data, [
            'exam_title' => 'required',
            'class_id' => 'required',
            'branch_id' => 'required',
        ]);
        $subjects = $data['exam_subject'];
        $sectionIds = $data['section_id'];
        $examData = [];
        $currentDate= Carbon::now()->isoFormat('YYYY-MM-DD');
        foreach ($sectionIds as $seckey=>$sectionId){
            foreach ($subjects as $subKey=>$subject){
                $examData[$seckey][$subKey] =  $this->processSubjects($subject);
                $examData[$seckey][$subKey]['exam_title'] = $data['exam_title'];
                $examData[$seckey][$subKey]['unique_id'] = $seckey.rand(0,100).$subKey;
                $examData[$seckey][$subKey]['type_id'] = $data['exam_type'];
                $examData[$seckey][$subKey]['class_id'] = $data['class_id'];
                $examData[$seckey][$subKey]['branch_id'] = $data['branch_id'];
                $examData[$seckey][$subKey]['start_date'] = $currentDate;
                $examData[$seckey][$subKey]['end_date'] = $currentDate;
                $examData[$seckey][$subKey]['section_id'] = $sectionId['section_id'];
                $examData[$seckey][$subKey]['term'] = $data['exam_term'];;
                $examData[$seckey][$subKey]['status'] = $data['exam_status'];
                $examData[$seckey][$subKey]['comments'] = $data['exam_comments'];

            }
        }
        $examData = array_merge(...$examData);
        if ($validator->fails()) {
            throw new InvalidArgumentException($validator->errors()->first());
        }
        return $this->examRepository->save($examData);

    }

    public function formatData($examData){
        $new_array = [];
        foreach($examData as $arrkey => $arrvalue)
        {
            foreach($arrvalue as $innerkey => $innervalue){

                foreach($innervalue as $subkey => $subvalue){

                    $new_array[$arrkey][$subkey] = $subvalue; // Change this line

                }
            }
        }
        return $new_array;
    }

    public function processSubjects($subject){
        $examData = [];
        $examData['lesson'] = $subject['exam_lesson'];
        $examData['mark_type_id'] = $subject['mark_type_id'];
        $examData['subject_id'] = $subject['subject_id'];
        $examData['topic'] = $subject['exam_topic'];
        $examData['code'] = $subject['exam_code'];
        return $examData;
    }

    public function getMasterData(){
        $examStatus[] = array('id'=>1,'exam_status'=>'Active');
        $examStatus[] = array('id'=>2,'exam_status'=>'InActive');
        $masterData['branchDetails'] =  $this->branchRepo->getAllBranches();
        $masterData['classDetails'] =  $this->classRepo->getAllClass();
        $masterData['examType']  =  $this->examTypeRepo->getAll();
        $masterData['markTypes']   =  $this->markTypeRepo->getAllPublishedMarkType();
        $masterData['sectionDetails']   =  $this->sectionRepo->getAllSection();
        $masterData['examSubjects']    =  $this->subjectsRepo->getAllPublishedSubjects();
        $masterData['termDetails']   =  $this->termRepo->getAll();
        $masterData['examLesson']   =  $this->lessonRepo->getAllPublishedLesson();
        $masterData['examStatus']   =  $examStatus;
        $masterData['examTopic']   =  $this->examTopicRepo->getAllPublishedTopic();;
        return $masterData;
    }

}
