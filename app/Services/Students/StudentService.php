<?php

namespace App\Services\Students;


use App\Models\Common\Addresses;
use App\Repositories\Management\studentRepository;
use App\Repositories\Students\StudentsRepository;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use InvalidArgumentException;

class StudentService
{
    /**
     * @var $studentRepo
     */
    protected $studentRepo;


    public function __construct()
    {
        $this->studentRepo = new StudentsRepository();

    }


    public function create($request)
    {
        $address = [];
        $input = $request->all();
        $input['unique_id'] = rand(0,10000);
        $request->request->add($input);
        $studentId =  $this->studentRepo->create($request);
        $address['parent_id'] = $studentId->id;
        $address['type'] = STUDENT_ADDRESS;
        $address['street'] = $input['street'];
        $address['city'] = $input['city'];
        $address['state'] = $input['state'];
        $address['zip'] = $input['zip'];
        $address['branch_id'] = null;
        Addresses::create($address);
        return $studentId;
    }

    /**
     * Delete post by id.
     *
     * @param $id
     * @return String
     */
    public function deleteById($id)
    {
        DB::beginTransaction();

        try {
            $post = $this->studentRepo->delete($id);

        } catch (Exception $e) {
            DB::rollBack();
            Log::info($e->getMessage());

            throw new InvalidArgumentException('Unable to ');
        }

        DB::commit();

        return $post;

    }

    /**
     * Get all post.
     *
     * @return String
     */
    public function getAll()
    {
        return $this->studentRepo->getAll();
    }

    /**
     * Get post by id.
     *
     * @param $id
     * @return String
     */
    public function getById($id)
    {
        return $this->studentRepo->getById($id);
    }

    /**
     * Update post data
     * Store to DB if there are no errors.
     *
     * @param array $data
     * @return String
     */
    public function updatePost($data, $id)
    {
        $validator = Validator::make($data, [
            'title' => 'bail|min:2',
            'description' => 'bail|max:255'
        ]);

        if ($validator->fails()) {
            throw new InvalidArgumentException($validator->errors()->first());
        }

        DB::beginTransaction();

        try {
            $post = $this->studentRepo->update($data, $id);

        } catch (Exception $e) {
            DB::rollBack();
            Log::info($e->getMessage());

            throw new InvalidArgumentException('Unable to update post data');
        }

        DB::commit();

        return $post;

    }

    /**
     * Validate post data.
     * Store to DB if there are no errors.
     *
     * @param array $data
     * @return String
     */


}
