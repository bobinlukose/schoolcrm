<?php

namespace App\Services\Classes;

use App\Repositories\Branch\BranchRepository;
use App\Repositories\Classes\ClassesRepository;
use App\Repositories\Classes\LessonRepository;
use App\Repositories\Exam\ExamRepository;
use App\Repositories\Exam\ExamTypeRepository;
use App\Repositories\MarkTypes\MarkTypesRepository;
use App\Repositories\Sections\SectionsRepository;
use App\Repositories\Subjects\SubjectsRepository;
use App\Repositories\Terms\TermsRepository;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use InvalidArgumentException;

class LessonService
{
    /**
     * @var $examRepository
     */
    protected $lessonRepository;

    /**
     * PostService constructor.
     *
     * @param LessonRepository $lessonRepository
     */
    public function __construct(LessonRepository $lessonRepository)
    {
        $this->lessonRepository = $lessonRepository;
    }

    /**
     * Delete post by id.
     *
     * @param $id
     * @return String
     */
    public function deleteById($id)
    {
        DB::beginTransaction();

        try {
            $post = $this->lessonRepository->delete($id);

        } catch (Exception $e) {
            DB::rollBack();
            Log::info($e->getMessage());

            throw new InvalidArgumentException('Unable to ');
        }

        DB::commit();

        return $post;

    }

    /**
     * Get all post.
     *
     * @return String
     */
    public function getAll()
    {
        return $this->lessonRepository->getAll();
    }

    public function getAllSubjects()
    {
        return $this->lessonRepository->getAllSubjects();
    }

    /**
     * Get post by id.
     *
     * @param $id
     * @return String
     */
    public function getById($id)
    {
        return $this->lessonRepository->getById($id);
    }

    /**
     * Update post data
     * Store to DB if there are no errors.
     *
     * @param array $data
     * @return String
     */
    public function updatePost($data, $id)
    {
        $validator = Validator::make($data, [
            'title' => 'bail|min:2',
            'description' => 'bail|max:255'
        ]);

        if ($validator->fails()) {
            throw new InvalidArgumentException($validator->errors()->first());
        }

        DB::beginTransaction();

        try {
            $post = $this->lessonRepository->update($data, $id);

        } catch (Exception $e) {
            DB::rollBack();
            Log::info($e->getMessage());

            throw new InvalidArgumentException('Unable to update post data');
        }

        DB::commit();

        return $post;

    }

    /**
     * Validate post data.
     * Store to DB if there are no errors.
     *
     * @param array $data
     * @return String
     */
    public function create($data)
    {
        $data['branch_id'] = 1;
        $data['class_id'] = 1;
        $data['unique_id'] = $data['name'];
        return $this->lessonRepository->save($data);

    }


}
