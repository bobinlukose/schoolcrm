<?php

namespace App\Services\Classes;

use App\Repositories\Branch\BranchRepository;
use App\Repositories\Classes\ClassesRepository;
use App\Repositories\Exam\ExamRepository;
use App\Repositories\Exam\ExamTypeRepository;
use App\Repositories\MarkTypes\MarkTypesRepository;
use App\Repositories\Sections\SectionsRepository;
use App\Repositories\Terms\TermsRepository;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use InvalidArgumentException;

class MarkTypeService
{
    /**
     * @var $markTypeRepository
     */
    protected $markTypeRepository;

    /**
     * PostService constructor.
     *
     * @param MarkTypesRepository $markTypeRepository
     */
    public function __construct(MarkTypesRepository $markTypeRepository)
    {
        $this->markTypeRepository = $markTypeRepository;
    }

    /**
     * Delete post by id.
     *
     * @param $id
     * @return String
     */
    public function deleteById($id)
    {
        DB::beginTransaction();

        try {
            $post = $this->markTypeRepository->delete($id);

        } catch (Exception $e) {
            DB::rollBack();
            Log::info($e->getMessage());

            throw new InvalidArgumentException('Unable to ');
        }

        DB::commit();

        return $post;

    }

    /**
     * Get all post.
     *
     * @return String
     */
    public function getAll()
    {
        return $this->markTypeRepository->getAll();
    }

    public function getAllSubjects()
    {
        return $this->markTypeRepository->getAllSubjects();
    }

    /**
     * Get post by id.
     *
     * @param $id
     * @return String
     */
    public function getById($id)
    {
        return $this->markTypeRepository->getById($id);
    }

    /**
     * Update post data
     * Store to DB if there are no errors.
     *
     * @param array $data
     * @return String
     */
    public function updatePost($data, $id)
    {
        $validator = Validator::make($data, [
            'title' => 'bail|min:2',
            'description' => 'bail|max:255'
        ]);

        if ($validator->fails()) {
            throw new InvalidArgumentException($validator->errors()->first());
        }

        DB::beginTransaction();

        try {
            $post = $this->markTypeRepository->update($data, $id);

        } catch (Exception $e) {
            DB::rollBack();
            Log::info($e->getMessage());

            throw new InvalidArgumentException('Unable to update post data');
        }

        DB::commit();

        return $post;

    }

    /**
     * Validate post data.
     * Store to DB if there are no errors.
     *
     * @param array $data
     * @return String
     */
    public function create($data)
    {
        $data['branch_id'] = 1;
        $data['class_id'] = 1;
        $data['unique_id'] = $data['title'];
        $data['order'] = 1;
        $data['entry_type'] = 1;
        $data['status'] = 1;
        return $this->markTypeRepository->save($data);

    }


}
