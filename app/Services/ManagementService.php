<?php

namespace App\Services;


use App\Models\Common\Addresses;
use App\Repositories\Management\ManagementRepository;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use InvalidArgumentException;

class ManagementService
{
    /**
     * @var $managementRepo
     */
    protected $managementRepo;


    public function __construct()
    {
        $this->managementRepo = new ManagementRepository();

    }


    public function create($request)
    {
        $input = $request->all();

        $input['unique_id'] = rand(0,10000);
        $input['parent_id'] = NULL;
        $request->request->add($input);
        $managementId =  $this->managementRepo->create($request);
        $address['parent_id'] = $managementId->id;
        $address['type'] = MANAGEMENT_ADDRESS;
        $address['street'] = $input['street'];
        $address['city'] = $input['city'];
        $address['state'] = $input['state'];
        $address['zip'] = $input['zip'];
        $address['branch_id'] = null;
        Addresses::create($address);
        return $managementId;
    }

    /**
     * Delete post by id.
     *
     * @param $id
     * @return String
     */
    public function deleteById($id)
    {
        DB::beginTransaction();

        try {
            $post = $this->examRepository->delete($id);

        } catch (Exception $e) {
            DB::rollBack();
            Log::info($e->getMessage());

            throw new InvalidArgumentException('Unable to ');
        }

        DB::commit();

        return $post;

    }

    /**
     * Get all post.
     *
     * @return String
     */
    public function getAll()
    {
        return $this->managementRepo->getAll();
    }

    /**
     * Get post by id.
     *
     * @param $id
     * @return String
     */
    public function getById($id)
    {
        return $this->examRepository->getById($id);
    }

    /**
     * Update post data
     * Store to DB if there are no errors.
     *
     * @param array $data
     * @return String
     */
    public function updatePost($data, $id)
    {
        $validator = Validator::make($data, [
            'title' => 'bail|min:2',
            'description' => 'bail|max:255'
        ]);

        if ($validator->fails()) {
            throw new InvalidArgumentException($validator->errors()->first());
        }

        DB::beginTransaction();

        try {
            $post = $this->examRepository->update($data, $id);

        } catch (Exception $e) {
            DB::rollBack();
            Log::info($e->getMessage());

            throw new InvalidArgumentException('Unable to update post data');
        }

        DB::commit();

        return $post;

    }

    /**
     * Validate post data.
     * Store to DB if there are no errors.
     *
     * @param array $data
     * @return String
     */


}
