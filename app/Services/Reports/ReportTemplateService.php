<?php

namespace App\Services\Reports;

use App\Mail\SendMail;
use App\Models\Classes\Subjects;
use App\Models\Students\StudentsModel;
use App\Models\User;
use App\Repositories\Branch\BranchRepository;
use App\Repositories\Classes\ClassesRepository;
use App\Repositories\Exam\ExamRepository;
use App\Repositories\Exam\ExamTypeRepository;
use App\Repositories\MarkTypes\MarkTypesRepository;
use App\Repositories\Reports\ReportTemplateRepository;
use App\Repositories\Sections\SectionsRepository;
use App\Repositories\Subjects\SubjectsRepository;
use App\Repositories\Terms\TermsRepository;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use InvalidArgumentException;
use Mail;
use PDF;
use Storage;


class ReportTemplateService
{
    /**
     * @var $reportTemplateRepository
     */
    protected $reportTemplateRepository,$subjectRepository;

    /**
     * PostService constructor.
     *
     * @param ReportTemplateRepository $reportTemplateRepository
     */
    public function __construct(ReportTemplateRepository $reportTemplateRepository)
    {
        $this->reportTemplateRepository = $reportTemplateRepository;
        $this->subjectRepository = new SubjectsRepository();
    }

    /**
     * Delete post by id.
     *
     * @param $id
     * @return String
     */
    public function deleteById($id)
    {
        DB::beginTransaction();

        try {
            $post = $this->reportTemplateRepository->delete($id);

        } catch (Exception $e) {
            DB::rollBack();
            Log::info($e->getMessage());

            throw new InvalidArgumentException('Unable to ');
        }

        DB::commit();

        return $post;

    }

    /**
     * Get all post.
     *
     * @return String
     */
    public function getAll()
    {
        return $this->reportTemplateRepository->getAll();
    }

    public function getAllSubjects()
    {
        return $this->reportTemplateRepository->getAllSubjects();
    }

    /**
     * Get post by id.
     *
     * @param $id
     * @return String
     */
    public function getById($id)
    {
        return $this->reportTemplateRepository->getById($id);
    }

    /**
     * Update post data
     * Store to DB if there are no errors.
     *
     * @param array $data
     * @return String
     */
    public function update($data)
    {
        return $this->reportTemplateRepository->update($data);

    }

    /**
     * Validate post data.
     * Store to DB if there are no errors.
     *
     * @param array $data
     * @return String
     */
    public function create($data)
    {
        $data['branch_id'] = 1;
        $data['class_id'] = 1;
        $data['path'] = '/';
        return $this->reportTemplateRepository->save($data);

    }

    public function generateStudentReport($request)
    {

        $rules = $request['grades'];
        $studentId = $request['student_id'];
        $studentName = StudentsModel::find($studentId);
        $current_timestamp = Carbon::now()->timestamp;
        $pdfData['markList'] = $this->getDataForReport($request['exam_subject'],$rules);
        $pdfData['studentName'] = $studentName->first_name;
        $pdfData['gradeRules'] = $rules;

        //   $pdfData = array_merge(...$pdfData);

        $pdf = PDF::loadView('myPDF', $pdfData, ['debug' => true]);
        $fileName = $current_timestamp.'.pdf';
        $filePath = 'public/pdfs/' . $fileName;

        // Save the PDF to a storage folder
        Storage::disk('local')->put($filePath, $pdf->output());

        $this->sendEmailWithPDF($pdf);

    }

    public function generatePDF()
    {
        $current_timestamp = Carbon::now()->timestamp;

        $pdfData['markList'] = $this->getDataForReport();

     //   $pdfData = array_merge(...$pdfData);

        $pdf = PDF::loadView('myPDF', $pdfData, ['debug' => true]);
        $fileName = $current_timestamp.'.pdf';
        $filePath = 'public/pdfs/' . $fileName;

        // Save the PDF to a storage folder
        Storage::disk('local')->put($filePath, $pdf->output());

        $this->sendEmailWithPDF($pdf);

    }


    public function getDataForReport($subjects,$rules)
    {

        $reportData = [];
        foreach ($subjects as $key=>$subject){
            $key = $key+1;
            $reportData[$key]['subject_'.$key] = $subject['name'];
            $reportData[$key]['behaviour_'.$key] = $subject['behaviour'];
            $reportData[$key]['effort_'.$key] = $subject['effort'];
            $reportData[$key]['classwork_'.$key] =  $subject['classwork'];
            $reportData[$key]['exam_'.$key] = $subject['exam'];
            $studentMark = $reportData[$key]['behaviour_'.$key] +  $reportData[$key]['effort_'.$key]+ $reportData[$key]['classwork_'.$key]  + $reportData[$key]['exam_'.$key];
            $reportData[$key]['total_'.$key] = $studentMark;
            $gradeDetails = $this->getGrade($studentMark,$rules);
            $reportData[$key]['grade_'.$key] = $gradeDetails['grade'];
            $reportData[$key]['total_bg_'.$key] = $gradeDetails['color'];
        }
      return $reportData;
    }

    function calculateGrade($mark, $gradeRanges) {
       $gradeDetails = [];
       $gradeDetails['grade']  = 'F'; // Default grade if no range matches
       $gradeDetails['color']  = ''; // Default grade if no range matches
       foreach ($gradeRanges as $grade => $range) {
            $minRange = $range[0];
            $maxRange = $range[1];
            if ($mark >= $minRange && $mark <= $maxRange) {
                $gradeDetails['grade'] =  $grade;
                $gradeDetails['color'] =  $this->generateGradeColor($grade);
            }
        }
        return $gradeDetails;
    }

    public function generateGradeColor($grade) {
        $color = '';
        switch ($grade) {
            case 'A':
                $color = '#00ff00'; // Green
                break;
            case 'B':
                $color = '#ffff00'; // Yellow
                break;
            case 'C':
                $color = '#ffa500'; // Orange
                break;
            case 'D':
                $color = '#ff0000'; // Red
                break;
            default:
                $color = '#000000'; // Black (default color for unrecognized grades)
                break;
        }

        return $color;
    }



    public function getGrade($studentMark,$rules){
        $gradeRanges = [];
        foreach ($rules as $rule){
            $gradeRanges[$rule['grade']] = [ $rule['minimum_marks'], $rule['max_marks'] ];
        }
        return $this->calculateGrade($studentMark, $gradeRanges);
    }

    public function generateTemplate($mailData)
    {
        $mailData = array_merge(...$mailData);

        Mail::to('vipinlalrv@gmail.com')->send(new SendMail($mailData));


    }


    public function sendEmailWithPDF($pdf)
    {
        $ccEmails = ["vipinlalrv@gmail.com", "bobinlukose@gmail.com"];
        $data["recipientEmail"] = $ccEmails;
       // $data["recipientEmail"] ='bobinlukose@gmail.com';
        $data["title"] = "Hala Campus";
        $data["body"] = "Report Card";
        $data["pdf"] = $pdf;
        Mail::to($data["recipientEmail"])->send(new SendMail($data));

        return true;
    }


}
