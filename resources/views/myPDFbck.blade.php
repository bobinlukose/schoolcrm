<html>
<head>
    <base href="https://dashboard.emailjs.com/admin/templates/yb0euua/">
    <link type="text/css" rel="stylesheet" href="https://dashboard.emailjs.com/skins/ui/EmailJS/content.min.css" crossorigin="anonymous">
    <link type="text/css" rel="stylesheet" href="https://dashboard.emailjs.com/skins/content/EmailJS/content.min.css" crossorigin="anonymous">
</head>
<body id="tinymce" class="mce-content-body " data-autofill-highlight="false">
<p style="text-align: center;"><strong>YEAR 7 TERM EXAM REPORT 2022-23</strong></p>
<p style="text-align: left;"><strong>NAME : {{$studentName}}</strong></p>
<p style="text-align: left;">&nbsp;</p>
<table style="border-collapse: collapse; width: 100%; height: 216px;" border="1">
    <colgroup>
        <col style="width: 14.2722%;">
        <col style="width: 14.2722%;">
        <col style="width: 14.2722%;">
        <col style="width: 14.2722%;">
        <col style="width: 14.2722%;">
        <col style="width: 14.2722%;">
        <col style="width: 14.2722%;">
    </colgroup>
    <tbody>
    <tr style="height: 18px;">
        <td style="text-align: center; height: 18px;"><strong>SUBJECT</strong></td>
        <td style="text-align: center; height: 18px;"><strong>BEHAVIOUR(/5)</strong></td>
        <td style="text-align: center; height: 18px;"><strong>EFFORT(/5)</strong></td>
        <td style="text-align: center; height: 18px;"><strong>CLASSWORK(/40)</strong></td>
        <td style="text-align: center; height: 18px;"><strong>EXAM(/50)</strong></td>
        <td style="text-align: center; height: 18px;"><strong>TOTAL</strong></td>
        <td style="text-align: center; height: 18px;"><strong>GRADE</strong></td>
    </tr>
    @foreach($markList as $key => $result)
    <tr style="height: 18px;">
        <td style="text-align: center; height: 18px;">{{$result['subject_'.$key]}}</td>
        <td style="text-align: center; height: 18px;">{{$result['behaviour_'.$key]}}</td>
        <td style="text-align: center; height: 18px;">{{$result['effort_'.$key]}}</td>
        <td style="text-align: center; height: 18px;">{{$result['classwork_'.$key]}}</td>
        <td style="text-align: center; height: 18px;">{{$result['exam_'.$key]}}</td>
        <td style="text-align: center; height: 18px;background-color: {{$result['total_bg_'.$key]}}">{{$result['total_'.$key]}}</td>
        <td style="text-align: center; height: 18px;">{{$result['grade_'.$key]}}</td>
    </tr>
    @endforeach

    </tbody>
</table>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>


</body>
</html>
