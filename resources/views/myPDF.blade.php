<html>
<head>

    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
</head>


<body>
<div id="invoice">
    <div class="invoice overflow-auto" style="position: relative;background-color: #FFF;min-height: 680px;padding: 15px">
        <div style="min-width: 600px">
            <header style=" padding: 10px 0;
            margin-bottom: 20px;
            border-bottom: 1px solid #3989c6">

                <div class="row">

                        <img style="float: left;width: 10%" width="10%" src="https://static.vecteezy.com/system/resources/previews/011/514/843/original/judicial-quill-writing-on-open-book-judgment-certificate-or-police-document-education-book-quill-template-design-free-vector.jpg" data-holder-rendered="true" />
                        <img style="float: right;width: 10%"  width="10%"  src="https://classdigest.com/wp-content/uploads/2020/07/bs.png" data-holder-rendered="true" />


                </div>
            </header>

            <br>
            <main style=" padding-bottom: 50px;margin-top: 10px;">
                <div class="col invoice-to">
                    <div class="text-gray-light" style="text-align: center">YEAR 7 TERM EXAM REPORT 2022-23</div>

                </div>
                <div class="col invoice-to">
                    <div class="text-gray-light">NAME : {{$studentName}}</div>

                </div>



                <hr>
                <table border="1px solid black"  width="100%">
                    <thead>
                    <tr>
                        <th class="no">SUBJECT</th>
                        <th class="text-left">BEHAVIOUR(/5)</th>
                        <th class="text-right">EFFORT(/5)</th>
                        <th class="text-right">CLASSWORK(/40)</th>
                        <th class="text-right">EXAM(/50)</th>
                        <th class="text-right">TOTAL</th>
                        <th class="text-right">GRADE</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($markList as $key => $result)
                        <tr style="height: 18px;">
                            <td class="no" style="text-align: center; height: 18px;">{{$result['subject_'.$key]}}</td>
                            <td class="text-left">{{$result['behaviour_'.$key]}}</td>
                            <td class="text-left">{{$result['effort_'.$key]}}</td>
                            <td class="text-left">{{$result['classwork_'.$key]}}</td>
                            <td class="text-left">{{$result['exam_'.$key]}}</td>
                            <td style="text-align: center; height: 18px;background-color: {{$result['total_bg_'.$key]}}" class="text-left">{{$result['total_'.$key]}}</td>
                            <td style="text-align: center; height: 18px;background-color: {{$result['total_bg_'.$key]}}" style="text-align: center; height: 18px;">{{$result['grade_'.$key]}}</td>
                        </tr>
                    @endforeach



                    </tbody>
                </table>

                <hr>
                <table border="1px solid black" width="100%">
                    <thead>
                    <tr>
                        <th colspan="6" class="text-center font-weight-bold">KEY</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td class="no" rowspan="2">BEHAVIOUR</td>
                        <td class="text-left">1</td>
                        <td class="text-left unit">2</td>
                        <td class="text-left">3</td>
                        <td class="text-left unit">4</td>
                        <td class="text-left">5</td>
                    </tr>
                    <tr>
                        <td class="text-left">Unacceptable</td>
                        <td class="text-left unit">Poor</td>
                        <td class="text-left">Satisfactory</td>
                        <td class="text-left unit">Good</td>
                        <td class="text-left">Excellent</td>
                    </tr>
                    <tr>
                        <td class="no" rowspan="2">EFFORT</td>
                        <td class="text-left">1</td>
                        <td class="text-left unit">2</td>
                        <td class="text-left">3</td>
                        <td class="text-left unit">4</td>
                        <td class="text-left">5</td>
                    </tr>
                    <tr>
                        <td class="text-left">Unacceptable</td>
                        <td class="text-left unit">Poor</td>
                        <td class="text-left">Satisfactory</td>
                        <td class="text-left unit">Good</td>
                        <td class="text-left">Excellent</td>
                    </tr>

                    <tr>
                        <td colspan="3" class="text-center font-weight-bold">CLASSWORK will be marked out of 40</td>
                        <td colspan="3"  class="text-center unit font-weight-bold"> GRADE BOUNDARIES</td>
                    </tr>

                    <tr>
                        <td colspan="3" class="text-center  font-weight-bold">EXAMS will be marked out of 50</td>
                        <td colspan="3"  class="text-center unit font-weight-bold">
                            <table  width="100%">
                                <tr>
                                @foreach($gradeRules as $key => $gradeRule)
                                        <td>
                                            <p class="grade text-center">{{$gradeRule['grade']}}</p>
                                            <p class="grade_value text-center">{{$gradeRule['max_marks']}}-{{$gradeRule['minimum_marks']}}</p>
                                        </td>
                                @endforeach
                                </tr>
                            </table>
                        </td>
                    </tr>
                    </tbody>
                </table>


                <hr>
                <table border="1px solid black"  width="100%">
                    <thead>
                    <tr>
                        <th colspan="2" class="text-left">NUMBER OF DAYS LATE</th>
                        <th colspan="4"  class="unit text-left">0</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td colspan="2" class="text-left">NUMBER OF DAYS ABSENT</td>
                        <td colspan="4"  class="unit text-left">0</td>
                    </tr>
                    <tr>
                        <td colspan="2" class="text-left">UNIFORM</td>
                        <td colspan="4"  class="unit text-left">GREEN</td>
                    </tr>
                    <tr>
                        <td colspan="2" class="text-left">NUMBER OF DETENTIONS</td>
                        <td colspan="4"  class="unit text-left">0</td>
                    </tr>
                    <tr>
                        <td colspan="2" class="text-left">NUMBER OF SUSPENSIONS</td>
                        <td colspan="4"  class="unit text-left">0</td>
                    </tr>
                    <tr>
                        <td colspan="2" class="text-left">COMMUNITY SERVICE</td>
                        <td colspan="4"  class="unit text-left">0</td>
                    </tr>
                    <tr>
                        <td colspan="2" class="text-left font-weight-bold">TUTOR COMMENT</td>
                        <td colspan="4"  class="unit text-left">0</td>
                    </tr>

                    <tr>
                        <td colspan="6" class="unit text-left"> Nik is very hard working, always listening and completing tasks given. Well done!</td>
                    </tr>

                    </tbody>
                </table>


                <hr>


                <div class="row contacts" style=" margin-bottom: 20px">
                    <div class="col invoice-to">
                        <div class="text-gray-light">Head of Secondary's signature:</div>

                    </div>
                    <div class="col invoice-details" style="text-align: right;margin-top: 0;
            color: #3989c6">
                        <div class="date">Date : 01/10/2018</div>
                    </div>
                </div>

                <div class="row contacts principal_sign" style=" margin-bottom: 20px">
                    <div class="col invoice-to">
                        <div class="text-gray-light">Principal's signature:</div>

                    </div>
                    <div class="col invoice-details" style="text-align: right;margin-top: 0;
            color: #3989c6">
                        <div class="date">Date : 01/10/2018</div>
                    </div>
                </div>


                <div class="white_space"></div>




            </main>
            <footer>
                --
            </footer>
        </div>
        <!--DO NOT DELETE THIS div. IT is responsible for showing footer always at the bottom-->
        <div></div>
    </div>
</div>
</body>
</html>
