import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const View = {template: '<router-view></router-view>'}

const routes = [
    {
        path: '/',
        component: () => import('@pages/Layout/DashboardLayout.vue'),
        redirect: 'dashboard',
        children: [
            {
                path: 'dashboard',
                name: 'dashboard',
                component: () => import('@pages/Dashboard.vue'),
                meta: {title: 'global.dashboard'}
            },
            {
                path: '/chatify',
                name: 'chatify',
                meta: {title: 'global.dashboard'},
               /* beforeEnter(to, from, next) {
                    window.location.replace("/admin/chatify")
                }*/
                component: () => import('@cruds/Chats/Index.vue'),
            },

            {
                path: 'user-management',
                name: 'user_management',
                component: View,
                redirect: {name: 'permissions.index'},
                children: [
                    {
                        path: 'permissions',
                        name: 'permissions.index',
                        component: () => import('@cruds/Permissions/Index.vue'),
                        meta: {title: 'cruds.permission.title'}
                    },
                    {
                        path: 'permissions/create',
                        name: 'permissions.create',
                        component: () => import('@cruds/Permissions/Create.vue'),
                        meta: {title: 'cruds.permission.title'}
                    },
                    {
                        path: 'permissions/:id',
                        name: 'permissions.show',
                        component: () => import('@cruds/Permissions/Show.vue'),
                        meta: {title: 'cruds.permission.title'}
                    },
                    {
                        path: 'permissions/:id/edit',
                        name: 'permissions.edit',
                        component: () => import('@cruds/Permissions/Edit.vue'),
                        meta: {title: 'cruds.permission.title'}
                    },
                    {
                        path: 'roles',
                        name: 'roles.index',
                        component: () => import('@cruds/Roles/Index.vue'),
                        meta: {title: 'cruds.role.title'}
                    },
                    {
                        path: 'roles/create',
                        name: 'roles.create',
                        component: () => import('@cruds/Roles/Create.vue'),
                        meta: {title: 'cruds.role.title'}
                    },
                    {
                        path: 'roles/:id',
                        name: 'roles.show',
                        component: () => import('@cruds/Roles/Show.vue'),
                        meta: {title: 'cruds.role.title'}
                    },
                    {
                        path: 'roles/:id/edit',
                        name: 'roles.edit',
                        component: () => import('@cruds/Roles/Edit.vue'),
                        meta: {title: 'cruds.role.title'}
                    },
                    {
                        path: 'users',
                        name: 'users.index',
                        component: () => import('@cruds/Users/Index.vue'),
                        meta: {title: 'cruds.user.title'}
                    },
                    {
                        path: 'users/create',
                        name: 'users.create',
                        component: () => import('@cruds/Users/Create.vue'),
                        meta: {title: 'cruds.user.title'}
                    },
                    {
                        path: 'users/:id',
                        name: 'users.show',
                        component: () => import('@cruds/Users/Show.vue'),
                        meta: {title: 'cruds.user.title'}
                    },
                    {
                        path: 'users/:id/edit',
                        name: 'users.edit',
                        component: () => import('@cruds/Users/Edit.vue'),
                        meta: {title: 'cruds.user.title'}
                    }
                ]
            },
            {
                path: 'product-management',
                name: 'product_management',
                component: View,
                redirect: {name: 'product_categories.index'},
                children: [
                    {
                        path: 'product-categories',
                        name: 'product_categories.index',
                        component: () => import('@cruds/ProductCategories/Index.vue'),
                        meta: {title: 'cruds.productCategory.title'}
                    },
                    {
                        path: 'product-categories/create',
                        name: 'product_categories.create',
                        component: () => import('@cruds/ProductCategories/Create.vue'),
                        meta: {title: 'cruds.productCategory.title'}
                    },
                    {
                        path: 'product-categories/:id',
                        name: 'product_categories.show',
                        component: () => import('@cruds/ProductCategories/Show.vue'),
                        meta: {title: 'cruds.productCategory.title'}
                    },
                    {
                        path: 'product-categories/:id/edit',
                        name: 'product_categories.edit',
                        component: () => import('@cruds/ProductCategories/Edit.vue'),
                        meta: {title: 'cruds.productCategory.title'}
                    },
                    {
                        path: 'product-tags',
                        name: 'product_tags.index',
                        component: () => import('@cruds/ProductTags/Index.vue'),
                        meta: {title: 'cruds.productTag.title'}
                    },
                    {
                        path: 'product-tags/create',
                        name: 'product_tags.create',
                        component: () => import('@cruds/ProductTags/Create.vue'),
                        meta: {title: 'cruds.productTag.title'}
                    },
                    {
                        path: 'product-tags/:id',
                        name: 'product_tags.show',
                        component: () => import('@cruds/ProductTags/Show.vue'),
                        meta: {title: 'cruds.productTag.title'}
                    },
                    {
                        path: 'product-tags/:id/edit',
                        name: 'product_tags.edit',
                        component: () => import('@cruds/ProductTags/Edit.vue'),
                        meta: {title: 'cruds.productTag.title'}
                    },
                    {
                        path: 'products',
                        name: 'products.index',
                        component: () => import('@cruds/Products/Index.vue'),
                        meta: {title: 'cruds.product.title'}
                    },
                    {
                        path: 'products/create',
                        name: 'products.create',
                        component: () => import('@cruds/Products/Create.vue'),
                        meta: {title: 'cruds.product.title'}
                    },
                    {
                        path: 'products/:id',
                        name: 'products.show',
                        component: () => import('@cruds/Products/Show.vue'),
                        meta: {title: 'cruds.product.title'}
                    },
                    {
                        path: 'products/:id/edit',
                        name: 'products.edit',
                        component: () => import('@cruds/Products/Edit.vue'),
                        meta: {title: 'cruds.product.title'}
                    }
                ]
            },
            {
                path: 'faq-management',
                name: 'faq_management',
                component: View,
                redirect: {name: 'faq_categories.index'},
                children: [
                    {
                        path: 'faq-categories',
                        name: 'faq_categories.index',
                        component: () => import('@cruds/FaqCategories/Index.vue'),
                        meta: {title: 'cruds.faqCategory.title'}
                    },
                    {
                        path: 'faq-categories/create',
                        name: 'faq_categories.create',
                        component: () => import('@cruds/FaqCategories/Create.vue'),
                        meta: {title: 'cruds.faqCategory.title'}
                    },
                    {
                        path: 'faq-categories/:id',
                        name: 'faq_categories.show',
                        component: () => import('@cruds/FaqCategories/Show.vue'),
                        meta: {title: 'cruds.faqCategory.title'}
                    },
                    {
                        path: 'faq-categories/:id/edit',
                        name: 'faq_categories.edit',
                        component: () => import('@cruds/FaqCategories/Edit.vue'),
                        meta: {title: 'cruds.faqCategory.title'}
                    },
                    {
                        path: 'faq-questions',
                        name: 'faq_questions.index',
                        component: () => import('@cruds/FaqQuestions/Index.vue'),
                        meta: {title: 'cruds.faqQuestion.title'}
                    },
                    {
                        path: 'faq-questions/create',
                        name: 'faq_questions.create',
                        component: () => import('@cruds/FaqQuestions/Create.vue'),
                        meta: {title: 'cruds.faqQuestion.title'}
                    },
                    {
                        path: 'faq-questions/:id',
                        name: 'faq_questions.show',
                        component: () => import('@cruds/FaqQuestions/Show.vue'),
                        meta: {title: 'cruds.faqQuestion.title'}
                    },
                    {
                        path: 'faq-questions/:id/edit',
                        name: 'faq_questions.edit',
                        component: () => import('@cruds/FaqQuestions/Edit.vue'),
                        meta: {title: 'cruds.faqQuestion.title'}
                    }
                ]
            },
            {
                path: 'school-management',
                name: 'school_management',
                component: View,
                redirect: {name: 'management.index'},
                children: [
                    {
                        path: 'management',
                        name: 'management.index',
                        component: () => import('@cruds/Management/Index.vue'),
                        meta: {title: 'cruds.managementDetails.title'}
                    },
                    {
                        path: 'management/create',
                        name: 'management.create',
                        component: () => import('@cruds/Management/Create.vue'),
                        meta: {title: 'cruds.managementDetails.title'}
                    },
                    {
                        path: 'management/:id',
                        name: 'management.show',
                        component: () => import('@cruds/Management/Show.vue'),
                        meta: {title: 'cruds.managementDetails.title'}
                    },
                    {
                        path: 'management/:id/edit',
                        name: 'management.edit',
                        component: () => import('@cruds/Management/Edit.vue'),
                        meta: {title: 'cruds.managementDetails.title'}
                    },
                    {
                        path: 'branch',
                        name: 'branch.index',
                        component: () => import('@cruds/Branch/Index.vue'),
                        meta: {title: 'cruds.branchManagement.title'}
                    },
                    {
                        path: 'branch/create',
                        name: 'branch.create',
                        component: () => import('@cruds/Branch/Create.vue'),
                        meta: {title: 'cruds.branchManagement.title'}
                    },
                    {
                        path: 'branch/:id',
                        name: 'branch.show',
                        component: () => import('@cruds/Branch/Show.vue'),
                        meta: {title: 'cruds.branchManagement.title'}
                    },
                    {
                        path: 'branch/:id/edit',
                        name: 'branch.edit',
                        component: () => import('@cruds/Branch/Edit.vue'),
                        meta: {title: 'cruds.branchManagement.title'}
                    },
                    {
                        path: 'classes',
                        name: 'classes.index',
                        component: () => import('@cruds/Classes/Index.vue'),
                        meta: {title: 'cruds.Classes.title'}
                    },
                    {
                        path: 'classes/create',
                        name: 'classes.create',
                        component: () => import('@cruds/Classes/Create.vue'),
                        meta: {title: 'cruds.Classes.title'}
                    },
                    {
                        path: 'classes/:id',
                        name: 'classes.show',
                        component: () => import('@cruds/Classes/Show.vue'),
                        meta: {title: 'cruds.Classes.title'}
                    },
                    {
                        path: 'classes/:id/edit',
                        name: 'classes.edit',
                        component: () => import('@cruds/Classes/Edit.vue'),
                        meta: {title: 'cruds.Classes.title'}
                    },
                    {
                        path: 'mark-type',
                        name: 'mark_type.index',
                        component: () => import('@cruds/MarkType/Index.vue'),
                        meta: {title: 'cruds.markType.title'}
                    },
                    {
                        path: 'mark-type/create',
                        name: 'mark_type.create',
                        component: () => import('@cruds/MarkType/Create.vue'),
                        meta: {title: 'cruds.markType.title'}
                    },
                    {
                        path: 'mark-type/:id',
                        name: 'mark_type.show',
                        component: () => import('@cruds/MarkType/Show.vue'),
                        meta: {title: 'cruds.markType.title'}
                    },
                    {
                        path: 'mark-type/:id/edit',
                        name: 'mark_type.edit',
                        component: () => import('@cruds/MarkType/Edit.vue'),
                        meta: {title: 'cruds.markType.title'}
                    },
                    {
                        path: 'section',
                        name: 'section.index',
                        component: () => import('@cruds/Section/Index.vue'),
                        meta: {title: 'cruds.section.title'}
                    },
                    {
                        path: 'section/create',
                        name: 'section.create',
                        component: () => import('@cruds/Section/Create.vue'),
                        meta: {title: 'cruds.section.title'}
                    },
                    {
                        path: 'section/:id',
                        name: 'section.show',
                        component: () => import('@cruds/Section/Show.vue'),
                        meta: {title: 'cruds.section.title'}
                    },
                    {
                        path: 'section/:id/edit',
                        name: 'section.edit',
                        component: () => import('@cruds/Section/Edit.vue'),
                        meta: {title: 'cruds.section.title'}
                    },
                ]
            },
            {
                path: 'content-management',
                name: 'content_management',
                component: View,
                redirect: {name: 'content_categories.index'},
                children: [
                    {
                        path: 'content-categories',
                        name: 'content_categories.index',
                        component: () => import('@cruds/ContentCategories/Index.vue'),
                        meta: {title: 'cruds.contentCategory.title'}
                    },
                    {
                        path: 'content-categories/create',
                        name: 'content_categories.create',
                        component: () => import('@cruds/ContentCategories/Create.vue'),
                        meta: {title: 'cruds.contentCategory.title'}
                    },
                    {
                        path: 'content-categories/:id',
                        name: 'content_categories.show',
                        component: () => import('@cruds/ContentCategories/Show.vue'),
                        meta: {title: 'cruds.contentCategory.title'}
                    },
                    {
                        path: 'content-categories/:id/edit',
                        name: 'content_categories.edit',
                        component: () => import('@cruds/ContentCategories/Edit.vue'),
                        meta: {title: 'cruds.contentCategory.title'}
                    },
                    {
                        path: 'content-tags',
                        name: 'content_tags.index',
                        component: () => import('@cruds/ContentTags/Index.vue'),
                        meta: {title: 'cruds.contentTag.title'}
                    },
                    {
                        path: 'content-tags/create',
                        name: 'content_tags.create',
                        component: () => import('@cruds/ContentTags/Create.vue'),
                        meta: {title: 'cruds.contentTag.title'}
                    },
                    {
                        path: 'content-tags/:id',
                        name: 'content_tags.show',
                        component: () => import('@cruds/ContentTags/Show.vue'),
                        meta: {title: 'cruds.contentTag.title'}
                    },
                    {
                        path: 'content-tags/:id/edit',
                        name: 'content_tags.edit',
                        component: () => import('@cruds/ContentTags/Edit.vue'),
                        meta: {title: 'cruds.contentTag.title'}
                    },
                    {
                        path: 'content-pages',
                        name: 'content_pages.index',
                        component: () => import('@cruds/ContentPages/Index.vue'),
                        meta: {title: 'cruds.contentPage.title'}
                    },
                    {
                        path: 'content-pages/create',
                        name: 'content_pages.create',
                        component: () => import('@cruds/ContentPages/Create.vue'),
                        meta: {title: 'cruds.contentPage.title'}
                    },
                    {
                        path: 'content-pages/:id',
                        name: 'content_pages.show',
                        component: () => import('@cruds/ContentPages/Show.vue'),
                        meta: {title: 'cruds.contentPage.title'}
                    },
                    {
                        path: 'content-pages/:id/edit',
                        name: 'content_pages.edit',
                        component: () => import('@cruds/ContentPages/Edit.vue'),
                        meta: {title: 'cruds.contentPage.title'}
                    }
                ]
            },
            {
                path: 'reports',
                name: 'reports',
                component: View,
                redirect: {name: 'report_templates.index'},
                children: [
                    {
                        path: 'report-templates',
                        name: 'report_templates.index',
                        component: () => import('@cruds/ReportTemplates/Index.vue'),
                        meta: {title: 'cruds.crmStatus.title'}
                    },
                    {
                        path: 'report-templates/create',
                        name: 'report_templates.create',
                        component: () => import('@cruds/ReportTemplates/Create.vue'),
                        meta: {title: 'cruds.crmStatus.title'}
                    },
                    {
                        path: 'report-templates/:id',
                        name: 'report_templates.show',
                        component: () => import('@cruds/ReportTemplates/Show.vue'),
                        meta: {title: 'cruds.crmStatus.title'}
                    },
                    {
                        path: 'report-templates/:id/edit',
                        name: 'report_templates.edit',
                        component: () => import('@cruds/ReportTemplates/Edit.vue'),
                        meta: {title: 'cruds.crmStatus.title'}
                    },


                    {
                        path: 'crm-customers',
                        name: 'crm_customers.index',
                        component: () => import('@cruds/CrmCustomers/Index.vue'),
                        meta: {title: 'cruds.crmCustomer.title'}
                    },

                    {
                        path: 'crm-customers/create',
                        name: 'crm_customers.create',
                        component: () => import('@cruds/CrmCustomers/Create.vue'),
                        meta: {title: 'cruds.crmCustomer.title'}
                    },
                    {
                        path: 'crm-customers/:id',
                        name: 'crm_customers.show',
                        component: () => import('@cruds/CrmCustomers/Show.vue'),
                        meta: {title: 'cruds.crmCustomer.title'}
                    },
                    {
                        path: 'crm-customers/:id/edit',
                        name: 'crm_customers.edit',
                        component: () => import('@cruds/CrmCustomers/Edit.vue'),
                        meta: {title: 'cruds.crmCustomer.title'}
                    },
                    {
                        path: 'crm-notes',
                        name: 'crm_notes.index',
                        component: () => import('@cruds/CrmNotes/Index.vue'),
                        meta: {title: 'cruds.crmNote.title'}
                    },
                    {
                        path: 'crm-notes/create',
                        name: 'crm_notes.create',
                        component: () => import('@cruds/CrmNotes/Create.vue'),
                        meta: {title: 'cruds.crmNote.title'}
                    },
                    {
                        path: 'crm-notes/:id',
                        name: 'crm_notes.show',
                        component: () => import('@cruds/CrmNotes/Show.vue'),
                        meta: {title: 'cruds.crmNote.title'}
                    },
                    {
                        path: 'crm-notes/:id/edit',
                        name: 'crm_notes.edit',
                        component: () => import('@cruds/CrmNotes/Edit.vue'),
                        meta: {title: 'cruds.crmNote.title'}
                    },
                    {
                        path: 'crm-documents',
                        name: 'crm_documents.index',
                        component: () => import('@cruds/CrmDocuments/Index.vue'),
                        meta: {title: 'cruds.crmDocument.title'}
                    },
                    {
                        path: 'crm-documents/create',
                        name: 'crm_documents.create',
                        component: () => import('@cruds/CrmDocuments/Create.vue'),
                        meta: {title: 'cruds.crmDocument.title'}
                    },
                    {
                        path: 'crm-documents/:id',
                        name: 'crm_documents.show',
                        component: () => import('@cruds/CrmDocuments/Show.vue'),
                        meta: {title: 'cruds.crmDocument.title'}
                    },
                    {
                        path: 'crm-documents/:id/edit',
                        name: 'crm_documents.edit',
                        component: () => import('@cruds/CrmDocuments/Edit.vue'),
                        meta: {title: 'cruds.crmDocument.title'}
                    }
                ]
            },
            {
                path: 'subjects',
                name: 'subjects.index',
                component: () => import('@cruds/Subjects/Index.vue'),
                meta: {title: 'cruds.subjects.title'}
            },
            {
                path: 'subjects/create',
                name: 'subjects.create',
                component: () => import('@cruds/Subjects/Create.vue'),
                meta: {title: 'cruds.subjects.title'}
            },
            {
                path: 'subjects/:id',
                name: 'subjects.show',
                component: () => import('@cruds/Subjects/Show.vue'),
                meta: {title: 'cruds.subjects.title'}
            },
            {
                path: 'subjects/:id/edit',
                name: 'subjects.edit',
                component: () => import('@cruds/Subjects/Edit.vue'),
                meta: {title: 'cruds.subjects.title'}
            },
            {
                path: 'lessons',
                name: 'lessons.index',
                component: () => import('@cruds/Lessons/Index.vue'),
                meta: {title: 'cruds.lesson.title'}
            },
            {
                path: 'lessons/create',
                name: 'lessons.create',
                component: () => import('@cruds/Lessons/Create.vue'),
                meta: {title: 'cruds.lesson.title'}
            },
            {
                path: 'lessons/:id',
                name: 'lessons.show',
                component: () => import('@cruds/Lessons/Show.vue'),
                meta: {title: 'cruds.lesson.title'}
            },
            {
                path: 'lessons/:id/edit',
                name: 'lessons.edit',
                component: () => import('@cruds/Lessons/Edit.vue'),
                meta: {title: 'cruds.lesson.title'}
            },
            {
                path: 'tests',
                name: 'tests.index',
                component: () => import('@cruds/Tests/Index.vue'),
                meta: {title: 'cruds.test.title'}
            },
            {
                path: 'tests/create',
                name: 'tests.create',
                component: () => import('@cruds/Tests/Create.vue'),
                meta: {title: 'cruds.test.title'}
            },
            {
                path: 'tests/:id',
                name: 'tests.show',
                component: () => import('@cruds/Tests/Show.vue'),
                meta: {title: 'cruds.test.title'}
            },
            {
                path: 'tests/:id/edit',
                name: 'tests.edit',
                component: () => import('@cruds/Tests/Edit.vue'),
                meta: {title: 'cruds.test.title'}
            },
            {
                path: 'questions',
                name: 'questions.index',
                component: () => import('@cruds/Questions/Index.vue'),
                meta: {title: 'cruds.question.title'}
            },
            {
                path: 'questions/create',
                name: 'questions.create',
                component: () => import('@cruds/Questions/Create.vue'),
                meta: {title: 'cruds.question.title'}
            },
            {
                path: 'questions/:id',
                name: 'questions.show',
                component: () => import('@cruds/Questions/Show.vue'),
                meta: {title: 'cruds.question.title'}
            },
            {
                path: 'questions/:id/edit',
                name: 'questions.edit',
                component: () => import('@cruds/Questions/Edit.vue'),
                meta: {title: 'cruds.question.title'}
            },
            {
                path: 'question-options',
                name: 'question_options.index',
                component: () => import('@cruds/QuestionOptions/Index.vue'),
                meta: {title: 'cruds.questionOption.title'}
            },
            {
                path: 'question-options/create',
                name: 'question_options.create',
                component: () => import('@cruds/QuestionOptions/Create.vue'),
                meta: {title: 'cruds.questionOption.title'}
            },
            {
                path: 'question-options/:id',
                name: 'question_options.show',
                component: () => import('@cruds/QuestionOptions/Show.vue'),
                meta: {title: 'cruds.questionOption.title'}
            },
            {
                path: 'question-options/:id/edit',
                name: 'question_options.edit',
                component: () => import('@cruds/QuestionOptions/Edit.vue'),
                meta: {title: 'cruds.questionOption.title'}
            },
            {
                path: 'test-results',
                name: 'test_results.index',
                component: () => import('@cruds/TestResults/Index.vue'),
                meta: {title: 'cruds.testResult.title'}
            },
            {
                path: 'test-results/create',
                name: 'test_results.create',
                component: () => import('@cruds/TestResults/Create.vue'),
                meta: {title: 'cruds.testResult.title'}
            },
            {
                path: 'test-results/:id',
                name: 'test_results.show',
                component: () => import('@cruds/TestResults/Show.vue'),
                meta: {title: 'cruds.testResult.title'}
            },
            {
                path: 'test-results/:id/edit',
                name: 'test_results.edit',
                component: () => import('@cruds/TestResults/Edit.vue'),
                meta: {title: 'cruds.testResult.title'}
            },
            {
                path: 'test-answers',
                name: 'test_answers.index',
                component: () => import('@cruds/TestAnswers/Index.vue'),
                meta: {title: 'cruds.testAnswer.title'}
            },
            {
                path: 'test-answers/create',
                name: 'test_answers.create',
                component: () => import('@cruds/TestAnswers/Create.vue'),
                meta: {title: 'cruds.testAnswer.title'}
            },
            {
                path: 'test-answers/:id',
                name: 'test_answers.show',
                component: () => import('@cruds/TestAnswers/Show.vue'),
                meta: {title: 'cruds.testAnswer.title'}
            },
            {
                path: 'test-answers/:id/edit',
                name: 'test_answers.edit',
                component: () => import('@cruds/TestAnswers/Edit.vue'),
                meta: {title: 'cruds.testAnswer.title'}
            },
            {
                path: 'exam-management',
                name: 'exam_management',
                component: View,
                redirect: {name: 'exam.index'},
                children: [
                    {
                        path: 'exam',
                        name: 'exam.index',
                        component: () => import('@cruds/Exam/Index.vue'),
                        meta: {title: 'cruds.exam.title'}
                    },
                    {
                        path: 'exam/create',
                        name: 'exam.create',
                        component: () => import('@cruds/Exam/Create.vue'),
                        meta: {title: 'cruds.exam.title'}
                    },
                    {
                        path: 'exam/:id',
                        name: 'exam.show',
                        component: () => import('@cruds/Exam/Show.vue'),
                        meta: {title: 'cruds.exam.title'}
                    },
                    {
                        path: 'exam/:id/edit',
                        name: 'exam.edit',
                        component: () => import('@cruds/Exam/Edit.vue'),
                        meta: {title: 'cruds.exam.title'}
                    },

                ]
            },
            {
                path: 'students-management',
                name: 'students_management',
                component: View,
                redirect: {name: 'students.index'},
                children: [
                    {
                        path: 'students',
                        name: 'students.index',
                        component: () => import('@cruds/Students/Index.vue'),
                        meta: {title: 'cruds.students.title'}
                    },
                    {
                        path: 'students/create',
                        name: 'students.create',
                        component: () => import('@cruds/Students/Create.vue'),
                        meta: {title: 'cruds.students.title'}
                    },
                    {
                        path: 'students/:id',
                        name: 'students.show',
                        component: () => import('@cruds/Students/Show.vue'),
                        meta: {title: 'cruds.students.title'}
                    },
                    {
                        path: 'students/:id/edit',
                        name: 'students.edit',
                        component: () => import('@cruds/Students/Edit.vue'),
                        meta: {title: 'cruds.students.title'}
                    },

                ]
            },
        ]
    }
]

export default new VueRouter({
    mode: 'history',
    base: '/admin',
    routes
})
