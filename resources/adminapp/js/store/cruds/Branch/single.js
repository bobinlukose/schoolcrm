function initialState() {
    return {
        entry: {
            id: null,
            management_id: null,
            name: '',
            city: '',
            state: '',
            street: '',
            phone_1: '',
            phone_2: '',
            email: '',
            website: '',
            address: '',
            zip: '',
            created_at: '',
            updated_at: '',
            deleted_at: ''
        },
        lists: {
            company: []
        },
        loading: false
    }
}

const route = 'branch-details'

const getters = {
    entry: state => state.entry,
    lists: state => state.lists,
    loading: state => state.loading
}

const actions = {
    storeData({commit, state, dispatch}) {
        commit('setLoading', true)
        dispatch('Alert/resetState', null, {root: true})

        return new Promise((resolve, reject) => {
            let params = objectToFormData(state.entry, {
                indices: true,
                booleansAsIntegers: true
            })
            axios
                .post(route, params)
                .then(response => {
                    resolve(response)
                })
                .catch(error => {
                    let message = error.response.data.message || error.message
                    let errors = error.response.data.errors

                    dispatch(
                        'Alert/setAlert',
                        {message: message, errors: errors, color: 'danger'},
                        {root: true}
                    )

                    reject(error)
                })
                .finally(() => {
                    commit('setLoading', false)
                })
        })
    },
    updateData({commit, state, dispatch}) {
        commit('setLoading', true)
        dispatch('Alert/resetState', null, {root: true})

        return new Promise((resolve, reject) => {
            let params = objectToFormData(state.entry, {
                indices: true,
                booleansAsIntegers: true
            })
            params.set('_method', 'PUT')
            axios
                .post(`${route}/${state.entry.id}`, params)
                .then(response => {
                    resolve(response)
                })
                .catch(error => {
                    let message = error.response.data.message || error.message
                    let errors = error.response.data.errors

                    dispatch(
                        'Alert/setAlert',
                        {message: message, errors: errors, color: 'danger'},
                        {root: true}
                    )

                    reject(error)
                })
                .finally(() => {
                    commit('setLoading', false)
                })
        })
    },
    setManagement({commit}, value) {
        commit('setManagement', value)
    },
    setBranchName({commit}, value) {
        commit('setBranchName', value)
    },
    setContactLastName({commit}, value) {
        commit('setContactLastName', value)
    },
    setContactPhone1({commit}, value) {
        commit('setContactPhone1', value)
    },
    setContactPhone2({commit}, value) {
        commit('setContactPhone2', value)
    },
    setContactEmail({commit}, value) {
        commit('setContactEmail', value)
    },
    setCompanyWebsite({commit}, value) {
        commit('setCompanyWebsite', value)
    },
    setContactAddress({commit}, value) {
        commit('setContactAddress', value)
    },
    setCompanyZip({commit}, value) {
        commit('setCompanyZip', value)
    },
    setCompanyCity({commit}, value) {
        commit('setCompanyCity', value)
    },
    setCompanyState({commit}, value) {
        commit('setCompanyState', value)
    },
    setCompanyStreet({commit}, value) {
        commit('setCompanyStreet', value)
    },
    setCreatedAt({commit}, value) {
        commit('setCreatedAt', value)
    },
    setUpdatedAt({commit}, value) {
        commit('setUpdatedAt', value)
    },
    setDeletedAt({commit}, value) {
        commit('setDeletedAt', value)
    },
    fetchCreateData({commit}) {
        axios.get(`${route}/create`).then(response => {
            commit('setLists', response.data.meta)
        })
    },
    fetchEditData({commit, dispatch}, id) {
        axios.get(`${route}/${id}/edit`).then(response => {
            commit('setEntry', response.data.data)
            commit('setLists', response.data.meta)
        })
    },
    fetchShowData({commit, dispatch}, id) {
        axios.get(`${route}/${id}`).then(response => {
            commit('setEntry', response.data.data)
        })
    },
    resetState({commit}) {
        commit('resetState')
    }
}

const mutations = {
    setEntry(state, entry) {
        state.entry = entry
    },
    setManagement(state, value) {
        state.entry.management_id = value
    },
    setBranchName(state, value) {
        state.entry.name = value
    },
    setContactLastName(state, value) {
        state.entry.last_name = value
    },
    setContactPhone1(state, value) {
        state.entry.phone_1 = value
    },
    setContactPhone2(state, value) {
        state.entry.phone_2 = value
    },
    setContactEmail(state, value) {
        state.entry.email = value
    },
    setCompanyWebsite(state, value) {
        state.entry.website = value
    },
    setContactAddress(state, value) {
        state.entry.address = value
    },
    setCompanyZip(state, value) {
        state.entry.zip = value
    },
    setCompanyCity(state, value) {
        state.entry.city = value
    },
    setCompanyState(state, value) {
        state.entry.state = value
    },
    setCompanyStreet(state, value) {
        state.entry.street = value
    },
    setCreatedAt(state, value) {
        state.entry.created_at = value
    },
    setUpdatedAt(state, value) {
        state.entry.updated_at = value
    },
    setDeletedAt(state, value) {
        state.entry.deleted_at = value
    },
    setLists(state, lists) {
        state.lists = lists
    },
    setLoading(state, loading) {
        state.loading = loading
    },
    resetState(state) {
        state = Object.assign(state, initialState())
    }
}

export default {
    namespaced: true,
    state: initialState,
    getters,
    actions,
    mutations
}
