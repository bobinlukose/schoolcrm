function initialState() {
  return {
    entry: {
      exam_title: '',
      branch_id: null,
      class_id: null,
      exam_type: [],
      exam_term: [],
      exam_status: [],
      exam_comments: '',
      exam_subject: [],
      exam_branch: [],
      section_type: [],
      section_id: [],
      type_id: [],
      term_id: [],
      term: [],

    },
    lists: {
       examType: [],
       sectionDetails: [],
       classDetails: [],
       termDetails: [],
       examStatus: [],
       branchDetails: [],
       examSubjects: [],
       markTypes: [],
       examLesson: [],
       examTopic: [],
       masterData: [],
    },
    loading: false
  }
}

const route = 'exam'

const getters = {
  entry: state => state.entry,
  lists: state => state.lists,
  loading: state => state.loading
}

const actions = {
  storeData({ commit, state, dispatch }) {
    commit('setLoading', true)
    dispatch('Alert/resetState', null, { root: true })

    return new Promise((resolve, reject) => {
      let params = objectToFormData(state.entry, {
        indices: true,
        booleansAsIntegers: true
      })
      axios
        .post(route, params)
        .then(response => {
          resolve(response)
        })
        .catch(error => {
          let message = error.response.data.message || error.message
          let errors = error.response.data.errors

          dispatch(
            'Alert/setAlert',
            { message: message, errors: errors, color: 'danger' },
            { root: true }
          )

          reject(error)
        })
        .finally(() => {
          commit('setLoading', false)
        })
    })
  },
  updateData({ commit, state, dispatch }) {
    commit('setLoading', true)
    dispatch('Alert/resetState', null, { root: true })

    return new Promise((resolve, reject) => {
      let params = objectToFormData(state.entry, {
        indices: true,
        booleansAsIntegers: true
      })
      params.set('_method', 'PUT')
      axios
        .post(`${route}/${state.entry.id}`, params)
        .then(response => {
          resolve(response)
        })
        .catch(error => {
          let message = error.response.data.message || error.message
          let errors = error.response.data.errors

          dispatch(
            'Alert/setAlert',
            { message: message, errors: errors, color: 'danger' },
            { root: true }
          )

          reject(error)
        })
        .finally(() => {
          commit('setLoading', false)
        })
    })
  },
  setExamTitle({ commit }, value) {
    commit('setExamTitle', value)
  },
  setExamType({ commit }, value) {
    commit('setExamType', value)
  },
  setBranchId({ commit }, value) {
    commit('setBranchId', value)
  },
  setExamCode({ commit }, value) {
    commit('setExamCode', value)
  },
  setExamSectionId({ commit }, value) {
    commit('setExamSectionId', value)
  },
  setExamTerm({ commit }, value) {
    commit('setExamTerm', value)
  },
  setExamMarkTypeId({ commit }, value) {
    commit('setExamMarkTypeId', value)
  },
  setExamStatus({ commit }, value) {
    commit('setExamStatus', value)
  },
  setExamComments({ commit }, value) {
    commit('setExamComments', value)
  },
  setExamClassId({ commit }, value) {
    commit('setExamClassId', value)
  },
  setExamSubject({ commit }, value) {
    commit('setExamSubject', value)
  },
  setExamLesson({ commit }, value) {
    commit('setExamLesson', value)
  },
  setExamTopic({ commit }, value) {
    commit('setExamTopic', value)
  },
  setRoles({ commit }, value) {
    commit('setRoles', value)
  },
  setRememberToken({ commit }, value) {
    commit('setRememberToken', value)
  },
  setCreatedAt({ commit }, value) {
    commit('setCreatedAt', value)
  },
  setUpdatedAt({ commit }, value) {
    commit('setUpdatedAt', value)
  },
  setDeletedAt({ commit }, value) {
    commit('setDeletedAt', value)
  },
  fetchCreateData({ commit }) {
    axios.get(`${route}/create`).then(response => {
      commit('setLists', response.data.meta)
    })
  },
  fetchEditData({ commit, dispatch }, id) {
    axios.get(`${route}/${id}/edit`).then(response => {
      commit('setEntry', response.data.data)
      commit('setLists', response.data.meta)
    })
  },
  fetchShowData({ commit, dispatch }, id) {
    axios.get(`${route}/${id}`).then(response => {
      commit('setEntry', response.data.data)
    })
  },
  resetState({ commit }) {
    commit('resetState')
  }
}

const mutations = {
  setEntry(state, entry) {
    state.entry = entry
  },
  setExamTitle(state, value) {
    state.entry.exam_title = value
  },
  setExamType(state, value) {
    state.entry.exam_type = value
  },
  setBranchId(state, value) {
    state.entry.branch_id = value
  },
  setExamCode(state, value) {
    state.entry.exam_code = value
  },
  setExamSectionId(state, value) {
    state.entry.section_id = value
  },
  setExamTerm(state, value) {
    state.entry.exam_term = value
  },
  setExamMarkTypeId(state, value) {
    state.entry.mark_type_id = value
  },
  setExamStatus(state, value) {
    state.entry.exam_status = value
  },
  setExamComments(state, value) {
    state.entry.exam_comments = value
  },
  setExamClassId(state, value) {
    state.entry.class_id = value
  },
  setExamSubject(state, value) {
      state.entry.exam_subject.push(value)
  },
  setExamLesson(state, value) {
    state.entry.exam_lesson = value
  },
  setExamTopic(state, value) {
    state.entry.exam_topic = value
  },
  setRoles(state, value) {
    state.entry.roles = value
  },
  setRememberToken(state, value) {
    state.entry.remember_token = value
  },
  setCreatedAt(state, value) {
    state.entry.created_at = value
  },
  setUpdatedAt(state, value) {
    state.entry.updated_at = value
  },
  setDeletedAt(state, value) {
    state.entry.deleted_at = value
  },
  setLists(state, lists) {
    state.lists = lists
  },
  setLoading(state, loading) {
    state.loading = loading
  },
  resetState(state) {
    state = Object.assign(state, initialState())
  }
}

export default {
  namespaced: true,
  state: initialState,
  getters,
  actions,
  mutations
}
