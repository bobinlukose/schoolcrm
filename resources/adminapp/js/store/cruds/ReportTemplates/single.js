function initialState() {
  return {
    entry: {
      id: null,
      template_name: '',
      template_html: '',
      class_id: '',
      student_id: '',
      created_at: '',
      updated_at: '',
      deleted_at: '',
      exam_subject: [],
      grades: [],
    },
    lists: {
      rawTemplate: '',
      subjects: [],
      classes: [],
      students: [],
      schoolSubjects: [],
      grades: [],
    },
    loading: false
  }
}

const route = 'template'

const getters = {
  entry: state => state.entry,
  lists: state => state.lists,
  loading: state => state.loading
}

const actions = {
  storeData({ commit, state, dispatch }) {
    commit('setLoading', true)
    dispatch('Alert/resetState', null, { root: true })

    return new Promise((resolve, reject) => {
      let params = objectToFormData(state.entry, {
        indices: true,
        booleansAsIntegers: true
      })
      axios
        .post(route, params)
        .then(response => {
          resolve(response)
        })
        .catch(error => {
          let message = error.response.data.message || error.message
          let errors = error.response.data.errors

          dispatch(
            'Alert/setAlert',
            { message: message, errors: errors, color: 'danger' },
            { root: true }
          )

          reject(error)
        })
        .finally(() => {
          commit('setLoading', false)
        })
    })
  },
  updateData({ commit, state, dispatch }) {
    commit('setLoading', true)
    dispatch('Alert/resetState', null, { root: true })

    return new Promise((resolve, reject) => {
      let params = objectToFormData(state.entry, {
        indices: true,
        booleansAsIntegers: true
      })
      params.set('_method', 'PUT')
      axios
        .post(`${route}/${state.entry.id}`, params)
        .then(response => {
          resolve(response)
        })
        .catch(error => {
          let message = error.response.data.message || error.message
          let errors = error.response.data.errors

          dispatch(
            'Alert/setAlert',
            { message: message, errors: errors, color: 'danger' },
            { root: true }
          )

          reject(error)
        })
        .finally(() => {
          commit('setLoading', false)
        })
    })
  },
  setName({ commit }, value) {
    commit('setName', value)
  },
  setTemplate({ commit }, value) {
    commit('setTemplate', value)
  },
  setClass({commit}, value) {
    commit('setClass', value)
  },
  setStudentId({commit}, value) {
    commit('setStudentId', value)
  },
  setExamSubject({ commit }, value) {
        commit('setExamSubject', value)
  },
  setInput({ commit }, value) {
      commit('setInput', value)
  },
  setDefaultRules({ commit }, value) {
      commit('setDefaultRules', value)
  },
  setRules({ commit }, value) {
      commit('setRules', value)
  },
  setCreatedAt({ commit }, value) {
    commit('setCreatedAt', value)
  },
  setUpdatedAt({ commit }, value) {
    commit('setUpdatedAt', value)
  },
  setDeletedAt({ commit }, value) {
    commit('setDeletedAt', value)
  },
  fetchEditData({ commit, dispatch }, id) {
    axios.get(`${route}/${id}/edit`).then(response => {
      commit('setEntry', response.data.data)
      commit('setLists', response.data.meta);
      commit('setDefaultRules', response.data.meta);
    })
  },
  fetchShowData({ commit, dispatch }, id) {
    axios.get(`${route}/${id}`).then(response => {
      commit('setEntry', response.data.data)
    })
  },
  resetState({ commit }) {
    commit('resetState')
  }
}

const mutations = {
  setEntry(state, entry) {
    state.entry = entry
  },
  setLists(state, lists) {
    state.lists = lists
  },
  setName(state, value) {
    state.entry.template_name = value
  },
  setTemplate(state, value) {
    state.entry.template_html = value
  },
  setClass(state, value) {
    state.entry.class_id = value
  },
  setStudentId(state, value) {
    state.entry.student_id = value
  },
  setExamSubject(state, value) {
        state.entry.exam_subject.push(value)
  },
  setDefaultRules(state, value) {
      state.entry.grades = value.grades;
  },

  setInput(state, value) {
      const index = value.index;
      const field = value.field;
      state.lists.schoolSubjects[index][field] = value.value;
      let jsonObject = JSON.parse(JSON.stringify(state.lists.schoolSubjects[index]));
      delete jsonObject['id'];
      delete jsonObject['name'];
      delete jsonObject['total'];
      let sum = 0;
      Object.keys(jsonObject).forEach(function(k, v){
          if (Number.isInteger(v)) {
              sum += parseInt(jsonObject[k]);
          }

      });
      state.lists.schoolSubjects[index]['total'] = sum;
      state.entry.exam_subject = state.lists.schoolSubjects;
  },
  setRules(state, value) {
      const index = value.index;
      const field = value.field;
      state.lists.grades[index][field] = value.value;
      state.entry.grades = state.lists.grades;
  },
  setCreatedAt(state, value) {
    state.entry.created_at = value
  },
  setUpdatedAt(state, value) {
    state.entry.updated_at = value
  },
  setDeletedAt(state, value) {
    state.entry.deleted_at = value
  },
  setLoading(state, loading) {
    state.loading = loading
  },
  resetState(state) {
    state = Object.assign(state, initialState())
  }
}

export default {
  namespaced: true,
  state: initialState,
  getters,
  actions,
  mutations
}
