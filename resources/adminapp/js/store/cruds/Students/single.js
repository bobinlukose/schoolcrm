function initialState() {
    return {
        entry: {
            id: null,
            branch_id: null,
            class_id: null,
            first_name: '',
            last_name: '',
            city: '',
            state: '',
            street: '',
            phone_1: '',
            phone_2: '',
            email: '',
            website: '',
            address: '',
            zip: '',
            created_at: '',
            updated_at: '',
            deleted_at: '',
            thumbnail: [],
        },
        lists: {
            branch: [],
            classes: [],
        },
        loading: false
    }
}

const route = 'students'

const getters = {
    entry: state => state.entry,
    lists: state => state.lists,
    loading: state => state.loading
}

const actions = {
    storeData({commit, state, dispatch}) {
        commit('setLoading', true)
        dispatch('Alert/resetState', null, {root: true})

        return new Promise((resolve, reject) => {
            let params = objectToFormData(state.entry, {
                indices: true,
                booleansAsIntegers: true
            })
            axios
                .post(route, params)
                .then(response => {
                    resolve(response)
                })
                .catch(error => {
                    let message = error.response.data.message || error.message
                    let errors = error.response.data.errors

                    dispatch(
                        'Alert/setAlert',
                        {message: message, errors: errors, color: 'danger'},
                        {root: true}
                    )

                    reject(error)
                })
                .finally(() => {
                    commit('setLoading', false)
                })
        })
    },
    updateData({commit, state, dispatch}) {
        commit('setLoading', true)
        dispatch('Alert/resetState', null, {root: true})

        return new Promise((resolve, reject) => {
            let params = objectToFormData(state.entry, {
                indices: true,
                booleansAsIntegers: true
            })
            params.set('_method', 'PUT')
            axios
                .post(`${route}/${state.entry.id}`, params)
                .then(response => {
                    resolve(response)
                })
                .catch(error => {
                    let message = error.response.data.message || error.message
                    let errors = error.response.data.errors

                    dispatch(
                        'Alert/setAlert',
                        {message: message, errors: errors, color: 'danger'},
                        {root: true}
                    )

                    reject(error)
                })
                .finally(() => {
                    commit('setLoading', false)
                })
        })
    },
    setBranch({commit}, value) {
        commit('setBranch', value)
    },
    setStudentFName({commit}, value) {
        commit('setStudentFName', value)
    },
    setStudentLName({commit}, value) {
        commit('setStudentLName', value)
    },
    setClass({commit}, value) {
        commit('setClass', value)
    },
    setContactLastName({commit}, value) {
        commit('setContactLastName', value)
    },
    setContactPhone1({commit}, value) {
        commit('setContactPhone1', value)
    },
    setContactPhone2({commit}, value) {
        commit('setContactPhone2', value)
    },
    setContactEmail({commit}, value) {
        commit('setContactEmail', value)
    },
    setCompanyWebsite({commit}, value) {
        commit('setCompanyWebsite', value)
    },
    setContactAddress({commit}, value) {
        commit('setContactAddress', value)
    },
    setCompanyZip({commit}, value) {
        commit('setCompanyZip', value)
    },
    setCompanyCity({commit}, value) {
        commit('setCompanyCity', value)
    },
    setCompanyState({commit}, value) {
        commit('setCompanyState', value)
    },
    setCompanyStreet({commit}, value) {
        commit('setCompanyStreet', value)
    },
    setCreatedAt({commit}, value) {
        commit('setCreatedAt', value)
    },
    setUpdatedAt({commit}, value) {
        commit('setUpdatedAt', value)
    },
    setDeletedAt({commit}, value) {
        commit('setDeletedAt', value)
    },
    fetchCreateData({commit}) {
        axios.get(`${route}/create`).then(response => {
            commit('setLists', response.data.meta)
        })
    },
    fetchEditData({commit, dispatch}, id) {
        axios.get(`${route}/${id}/edit`).then(response => {
            commit('setEntry', response.data.data)
            commit('setLists', response.data.meta)
        })
    },
    fetchShowData({commit, dispatch}, id) {
        axios.get(`${route}/${id}`).then(response => {
            commit('setEntry', response.data.data)
        })
    },
    resetState({commit}) {
        commit('resetState')
    },
    insertThumbnailFile({commit}, file) {
        commit('insertThumbnailFile', file)
    },
    removeThumbnailFile({commit}, file) {
        commit('removeThumbnailFile', file)
    },
}

const mutations = {
    setEntry(state, entry) {
        state.entry = entry
    },
    setBranch(state, value) {
        state.entry.branch_id = value
    },
    setStudentFName(state, value) {
        state.entry.first_name = value
    },
    setStudentLName(state, value) {
        state.entry.last_name = value
    },
    setClass(state, value) {
        state.entry.class_id = value
    },
    setContactLastName(state, value) {
        state.entry.last_name = value
    },
    setContactPhone1(state, value) {
        state.entry.phone_1 = value
    },
    setContactPhone2(state, value) {
        state.entry.phone_2 = value
    },
    setContactEmail(state, value) {
        state.entry.email = value
    },
    setCompanyWebsite(state, value) {
        state.entry.website = value
    },
    setContactAddress(state, value) {
        state.entry.address = value
    },
    setCompanyZip(state, value) {
        state.entry.zip = value
    },
    setCompanyCity(state, value) {
        state.entry.city = value
    },
    setCompanyState(state, value) {
        state.entry.state = value
    },
    setCompanyStreet(state, value) {
        state.entry.street = value
    },
    setCreatedAt(state, value) {
        state.entry.created_at = value
    },
    setUpdatedAt(state, value) {
        state.entry.updated_at = value
    },
    setDeletedAt(state, value) {
        state.entry.deleted_at = value
    },
    setLists(state, lists) {
        state.lists = lists
    },
    setLoading(state, loading) {
        state.loading = loading
    },
    resetState(state) {
        state = Object.assign(state, initialState())
    },
    insertThumbnailFile(state, file) {
        state.entry.thumbnail.push(file)
    },
    removeThumbnailFile(state, file) {
        state.entry.thumbnail = state.entry.thumbnail.filter(item => {
            return item.id !== file.id
        })
    },
}

export default {
    namespaced: true,
    state: initialState,
    getters,
    actions,
    mutations
}
