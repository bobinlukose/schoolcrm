function initialState() {
    return {
        entry: {
            id: null,
            name: '',
            address: '',
            street: '',
            phone_1: '',
            phone_2: '',
            website: '',
            email: '',
            city: '',
            state: '',
            zip: '',
            created_at: '',
            updated_at: '',
            deleted_at: '',
            thumbnail: [],
        },
        loading: false
    }
}

const route = 'management'

const getters = {
    entry: state => state.entry,
    loading: state => state.loading
}

const actions = {
    storeData({commit, state, dispatch}) {
        commit('setLoading', true)
        dispatch('Alert/resetState', null, {root: true})

        return new Promise((resolve, reject) => {
            let params = objectToFormData(state.entry, {
                indices: true,
                booleansAsIntegers: true
            })
            axios
                .post(route, params)
                .then(response => {
                    resolve(response)
                })
                .catch(error => {
                    let message = error.response.data.message || error.message
                    let errors = error.response.data.errors

                    dispatch(
                        'Alert/setAlert',
                        {message: message, errors: errors, color: 'danger'},
                        {root: true}
                    )

                    reject(error)
                })
                .finally(() => {
                    commit('setLoading', false)
                })
        })
    },
    updateData({commit, state, dispatch}) {
        commit('setLoading', true)
        dispatch('Alert/resetState', null, {root: true})

        return new Promise((resolve, reject) => {
            let params = objectToFormData(state.entry, {
                indices: true,
                booleansAsIntegers: true
            })
            params.set('_method', 'PUT')
            axios
                .post(`${route}/${state.entry.id}`, params)
                .then(response => {
                    resolve(response)
                })
                .catch(error => {
                    let message = error.response.data.message || error.message
                    let errors = error.response.data.errors

                    dispatch(
                        'Alert/setAlert',
                        {message: message, errors: errors, color: 'danger'},
                        {root: true}
                    )

                    reject(error)
                })
                .finally(() => {
                    commit('setLoading', false)
                })
        })
    },
    setCompanyName({commit}, value) {
        commit('setCompanyName', value)
    },
    setCompanyAddress({commit}, value) {
        commit('setCompanyAddress', value)
    },
    setCompanyStreet({commit}, value) {
        commit('setCompanyStreet', value)
    },
    setCompanyWebsite({commit}, value) {
        commit('setCompanyWebsite', value)
    },
    setCompanyEmail({commit}, value) {
        commit('setCompanyEmail', value)
    }, setCompanyCity({commit}, value) {
        commit('setCompanyCity', value)
    },
    setContactPhone1({commit}, value) {
        commit('setContactPhone1', value)
    },
    setContactPhone2({commit}, value) {
        commit('setContactPhone2', value)
    },
    setCompanyState({commit}, value) {
        commit('setCompanyState', value)
    }, setCompanyZip({commit}, value) {
        commit('setCompanyZip', value)
    },
    setCreatedAt({commit}, value) {
        commit('setCreatedAt', value)
    },
    setUpdatedAt({commit}, value) {
        commit('setUpdatedAt', value)
    },
    setDeletedAt({commit}, value) {
        commit('setDeletedAt', value)
    },
    fetchEditData({commit, dispatch}, id) {
        axios.get(`${route}/${id}/edit`).then(response => {
            commit('setEntry', response.data.data)
        })
    },
    fetchShowData({commit, dispatch}, id) {
        axios.get(`${route}/${id}`).then(response => {
            commit('setEntry', response.data.data)
        })
    },
    resetState({commit}) {
        commit('resetState')
    },
    insertThumbnailFile({commit}, file) {
        commit('insertThumbnailFile', file)
    },
    removeThumbnailFile({commit}, file) {
        commit('removeThumbnailFile', file)
    },
}

const mutations = {
    setEntry(state, entry) {
        state.entry = entry
    },
    setCompanyName(state, value) {
        state.entry.name = value
    },
    setCompanyAddress(state, value) {
        state.entry.address = value
    },
    setCompanyStreet(state, value) {
        state.entry.street = value
    },
    setCompanyWebsite(state, value) {
        state.entry.website = value
    },
    setCompanyEmail(state, value) {
        state.entry.email = value
    },
    setContactPhone1(state, value) {
        state.entry.phone_1 = value
    },
    setContactPhone2(state, value) {
        state.entry.phone_2 = value
    },
    setCompanyCity(state, value) {
        state.entry.city = value
    }, setCompanyState(state, value) {
        state.entry.state = value
    }, setCompanyZip(state, value) {
        state.entry.zip = value
    },
    setCreatedAt(state, value) {
        state.entry.created_at = value
    },
    setUpdatedAt(state, value) {
        state.entry.updated_at = value
    },
    setDeletedAt(state, value) {
        state.entry.deleted_at = value
    },
    setLoading(state, loading) {
        state.loading = loading
    },
    insertThumbnailFile(state, file) {
        state.entry.thumbnail.push(file)
    },
    removeThumbnailFile(state, file) {
        state.entry.thumbnail = state.entry.thumbnail.filter(item => {
            return item.id !== file.id
        })
    },
    resetState(state) {
        state = Object.assign(state, initialState())
    }
}

export default {
    namespaced: true,
    state: initialState,
    getters,
    actions,
    mutations
}
