import Vue from 'vue'
import Vuex from 'vuex'

import Alert from './modules/alert'
import I18NStore from './modules/i18n'

import PermissionsIndex from './cruds/Permissions'
import PermissionsSingle from './cruds/Permissions/single'
import RolesIndex from './cruds/Roles'
import RolesSingle from './cruds/Roles/single'
import UsersIndex from './cruds/Users'
import UsersSingle from './cruds/Users/single'
import ProductCategoriesIndex from './cruds/ProductCategories'
import ProductCategoriesSingle from './cruds/ProductCategories/single'
import ProductTagsIndex from './cruds/ProductTags'
import ProductTagsSingle from './cruds/ProductTags/single'
import ProductsIndex from './cruds/Products'
import ProductsSingle from './cruds/Products/single'
import FaqCategoriesIndex from './cruds/FaqCategories'
import FaqCategoriesSingle from './cruds/FaqCategories/single'
import FaqQuestionsIndex from './cruds/FaqQuestions'
import FaqQuestionsSingle from './cruds/FaqQuestions/single'
import ManagementIndex from './cruds/Management'
import ManagementSingle from './cruds/Management/single'
import BranchIndex from './cruds/Branch'
import BranchSingle from './cruds/Branch/single'
import StudentsSingle from './cruds/Students/single'
import StudentsIndex from './cruds/Students'
import ContentCategoriesIndex from './cruds/ContentCategories'
import ContentCategoriesSingle from './cruds/ContentCategories/single'
import ContentTagsIndex from './cruds/ContentTags'
import ContentTagsSingle from './cruds/ContentTags/single'
import ContentPagesIndex from './cruds/ContentPages'
import ContentPagesSingle from './cruds/ContentPages/single'
import ReportTemplatesIndex from './cruds/ReportTemplates'
import ReportTemplatesSingle from './cruds/ReportTemplates/single'
import CrmCustomersIndex from './cruds/CrmCustomers'
import CrmCustomersSingle from './cruds/CrmCustomers/single'
import CrmNotesIndex from './cruds/CrmNotes'
import CrmNotesSingle from './cruds/CrmNotes/single'
import CrmDocumentsIndex from './cruds/CrmDocuments'
import CrmDocumentsSingle from './cruds/CrmDocuments/single'
import IncomeCategoriesIndex from './cruds/IncomeCategories'
import IncomeCategoriesSingle from './cruds/IncomeCategories/single'
import IncomesIndex from './cruds/Incomes'
import IncomesSingle from './cruds/Incomes/single'
import ExpenseReports from './cruds/ExpenseReports'
import SubjectsIndex from './cruds/Subjects'
import SubjectSingle from './cruds/Subjects/single'
import ClassSingle from './cruds/Classes/single'
import ClassIndex from './cruds/Classes'
import LessonsIndex from './cruds/Lessons'
import LessonsSingle from './cruds/Lessons/single'
import SectionSingle from './cruds/Section/single'
import SectionIndex from './cruds/Section'
import TestsIndex from './cruds/Tests'
import TestsSingle from './cruds/Tests/single'
import QuestionsIndex from './cruds/Questions'
import QuestionsSingle from './cruds/Questions/single'
import QuestionOptionsIndex from './cruds/QuestionOptions'
import QuestionOptionsSingle from './cruds/QuestionOptions/single'
import TestResultsIndex from './cruds/TestResults'
import TestResultsSingle from './cruds/TestResults/single'
import TestAnswersIndex from './cruds/TestAnswers'
import TestAnswersSingle from './cruds/TestAnswers/single'
import ExamIndex from './cruds/Exam'
import ExamSingle from './cruds/Exam/single'
import MarkTypeIndex from './cruds/MarkType'
import MarkTypeSingle from './cruds/MarkType/single'

Vue.use(Vuex)

const debug = process.env.NODE_ENV !== 'production'

export default new Vuex.Store({
  modules: {
    Alert,
    I18NStore,
    PermissionsIndex,
    PermissionsSingle,
    RolesIndex,
    RolesSingle,
    UsersIndex,
    ExamIndex,
    UsersSingle,
    ExamSingle,
    ProductCategoriesIndex,
    ProductCategoriesSingle,
    ProductTagsIndex,
    ProductTagsSingle,
    ProductsIndex,
    ProductsSingle,
    FaqCategoriesIndex,
    FaqCategoriesSingle,
    FaqQuestionsIndex,
    FaqQuestionsSingle,
    ManagementIndex,
    ManagementSingle,
    BranchIndex,
    StudentsIndex,
    BranchSingle,
    StudentsSingle,
    ContentCategoriesIndex,
    ContentCategoriesSingle,
    ContentTagsIndex,
    ContentTagsSingle,
    ContentPagesIndex,
    ContentPagesSingle,
    ReportTemplatesIndex,
    ReportTemplatesSingle,
    CrmCustomersIndex,
    CrmCustomersSingle,
    CrmNotesIndex,
    CrmNotesSingle,
    CrmDocumentsIndex,
    CrmDocumentsSingle,
    IncomeCategoriesIndex,
    IncomeCategoriesSingle,
    IncomesIndex,
    IncomesSingle,
    ExpenseReports,
    SubjectsIndex,
    SubjectSingle,
    ClassSingle,
    ClassIndex,
    LessonsIndex,
    SectionIndex,
    LessonsSingle,
    SectionSingle,
    TestsIndex,
    TestsSingle,
    QuestionsIndex,
    QuestionsSingle,
    QuestionOptionsIndex,
    QuestionOptionsSingle,
    TestResultsIndex,
    TestResultsSingle,
    TestAnswersIndex,
    TestAnswersSingle,
    MarkTypeIndex,
    MarkTypeSingle
  },
  strict: debug
})
