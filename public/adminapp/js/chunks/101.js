(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[101],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/adminapp/js/cruds/ReportTemplates/Edit.vue?vue&type=script&lang=js&":
/*!***********************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/adminapp/js/cruds/ReportTemplates/Edit.vue?vue&type=script&lang=js& ***!
  \***********************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vuex__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vuex */ "./node_modules/vuex/dist/vuex.esm.js");
function _typeof(obj) { "@babel/helpers - typeof"; return _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (obj) { return typeof obj; } : function (obj) { return obj && "function" == typeof Symbol && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }, _typeof(obj); }
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); enumerableOnly && (symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; })), keys.push.apply(keys, symbols); } return keys; }
function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = null != arguments[i] ? arguments[i] : {}; i % 2 ? ownKeys(Object(source), !0).forEach(function (key) { _defineProperty(target, key, source[key]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)) : ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } return target; }
function _defineProperty(obj, key, value) { key = _toPropertyKey(key); if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }
function _toPropertyKey(arg) { var key = _toPrimitive(arg, "string"); return _typeof(key) === "symbol" ? key : String(key); }
function _toPrimitive(input, hint) { if (_typeof(input) !== "object" || input === null) return input; var prim = input[Symbol.toPrimitive]; if (prim !== undefined) { var res = prim.call(input, hint || "default"); if (_typeof(res) !== "object") return res; throw new TypeError("@@toPrimitive must return a primitive value."); } return (hint === "string" ? String : Number)(input); }

/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      status: '',
      activeField: '',
      schoolSubjects: []
    };
  },
  computed: _objectSpread({}, Object(vuex__WEBPACK_IMPORTED_MODULE_0__["mapGetters"])('ReportTemplatesSingle', ['entry', 'loading', 'lists'])),
  beforeDestroy: function beforeDestroy() {
    this.resetState();
  },
  watch: {
    '$route.params.id': {
      immediate: true,
      handler: function handler() {
        this.resetState();
        this.fetchEditData(this.$route.params.id);
      }
    }
  },
  methods: _objectSpread(_objectSpread({}, Object(vuex__WEBPACK_IMPORTED_MODULE_0__["mapActions"])('ReportTemplatesSingle', ['fetchEditData', 'updateData', 'resetState', 'setName', 'setTemplate', 'setClass', 'setStudentId', 'setExamSubject', 'setDefaultRules', 'setInput', 'setRules'])), {}, {
    updateName: function updateName(e) {
      this.setName(e.target.value);
    },
    updateTemplate: function updateTemplate(e) {
      this.setTemplate(e.target.value);
    },
    updateClass: function updateClass(value) {
      this.setClass(value);
    },
    updateStudentId: function updateStudentId(value) {
      this.setStudentId(value);
    },
    updateInput: function updateInput(e, index) {
      var data = {};
      data.value = e.target.value;
      data.index = index;
      data.field = e.target.name;
      ;
      this.setInput(data);
    },
    updateRules: function updateRules(e, index) {
      var data = {};
      data.value = e.target.value;
      data.index = index;
      data.field = e.target.name;
      ;
      this.setRules(data);
    },
    submitForm: function submitForm() {
      var _this = this;
      this.updateData().then(function () {
        _this.$router.push({
          name: 'report_templates.index'
        });
        _this.$eventHub.$emit('update-success');
      })["catch"](function (error) {
        _this.status = 'failed';
        _.delay(function () {
          _this.status = '';
        }, 3000);
      });
    },
    focusField: function focusField(name) {
      this.activeField = name;
    },
    clearFocus: function clearFocus() {
      this.activeField = '';
    },
    updateExamSubject: function updateExamSubject(e, index) {
      this.examSubjects[index].subject_id = e.subject_id;
    }
  })
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/adminapp/js/cruds/ReportTemplates/Edit.vue?vue&type=template&id=189e5722&":
/*!*********************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib/loaders/templateLoader.js??ref--6!./node_modules/vue-loader/lib??vue-loader-options!./resources/adminapp/js/cruds/ReportTemplates/Edit.vue?vue&type=template&id=189e5722& ***!
  \*********************************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function render() {
  var _vm = this,
    _c = _vm._self._c;
  return _c("div", {
    staticClass: "container-fluid"
  }, [_c("form", {
    on: {
      submit: function submit($event) {
        $event.preventDefault();
        return _vm.submitForm.apply(null, arguments);
      }
    }
  }, [_c("div", {
    staticClass: "row"
  }, [_c("div", {
    staticClass: "col-md-12"
  }, [_c("div", {
    staticClass: "card"
  }, [_c("div", {
    staticClass: "card-header card-header-primary card-header-icon"
  }, [_vm._m(0), _vm._v(" "), _c("h4", {
    staticClass: "card-title"
  }, [_vm._v("\n              " + _vm._s(_vm.$t("global.edit")) + "\n              "), _c("strong", [_vm._v(_vm._s(_vm.$t("cruds.reportManagement.title_singular")))])])]), _vm._v(" "), _c("div", {
    staticClass: "card-body"
  }, [_c("back-button")], 1), _vm._v(" "), _c("div", {
    staticClass: "card-body"
  }, [_c("bootstrap-alert"), _vm._v(" "), _c("div", {
    staticClass: "row"
  }, [_c("div", {
    staticClass: "col-md-12"
  }, [_c("div", {
    staticClass: "form-group row"
  }, [_c("div", {
    staticClass: "col-sm-6"
  }, [_c("div", {
    staticClass: "form-group bmd-form-group",
    "class": {
      "has-items": _vm.entry.class_id !== null,
      "is-focused": _vm.activeField == "class_id"
    }
  }, [_c("label", {
    staticClass: "bmd-label-floating required"
  }, [_vm._v(_vm._s(_vm.$t("cruds.Classes.fields.title")))]), _vm._v(" "), _c("v-select", {
    key: "course-field",
    attrs: {
      name: "class_id",
      label: "name",
      value: _vm.entry.class_id,
      options: _vm.lists.classes,
      reduce: function reduce(entry) {
        return entry.id;
      }
    },
    on: {
      input: _vm.updateClass,
      search: [function ($event) {
        if (!$event.type.indexOf("key") && _vm._k($event.keyCode, "focus", undefined, $event.key, undefined)) return null;
        return _vm.focusField("classes");
      }, function ($event) {
        if (!$event.type.indexOf("key") && _vm._k($event.keyCode, "blur", undefined, $event.key, undefined)) return null;
        return _vm.clearFocus.apply(null, arguments);
      }]
    }
  })], 1)]), _vm._v(" "), _c("div", {
    staticClass: "col-sm-6"
  }, [_c("div", {
    staticClass: "form-group bmd-form-group",
    "class": {
      "has-items": _vm.entry.student_id !== 0,
      "is-focused": _vm.activeField == "student_id"
    }
  }, [_c("label", {
    staticClass: "bmd-label-floating required"
  }, [_vm._v(_vm._s(_vm.$t("cruds.studentManagement.fields.title")))]), _vm._v(" "), _c("v-select", {
    key: "exam-type-field",
    attrs: {
      name: "student_id",
      label: "first_name",
      value: _vm.entry.student_id,
      options: _vm.lists.students,
      closeOnSelect: false,
      reduce: function reduce(entry) {
        return entry.id;
      }
    },
    on: {
      input: _vm.updateStudentId,
      search: [function ($event) {
        if (!$event.type.indexOf("key") && _vm._k($event.keyCode, "focus", undefined, $event.key, undefined)) return null;
        return _vm.focusField("student_id");
      }, function ($event) {
        if (!$event.type.indexOf("key") && _vm._k($event.keyCode, "blur", undefined, $event.key, undefined)) return null;
        return _vm.clearFocus.apply(null, arguments);
      }]
    }
  })], 1)])]), _vm._v(" "), _c("div", {
    staticClass: "row"
  }, [_c("div", {
    staticClass: "content col-md-12"
  }, [_c("form", {
    on: {
      submit: function submit($event) {
        $event.preventDefault();
      }
    }
  }, [_c("table", {
    staticClass: "table table-hover table-striped w-100"
  }, [_c("thead", [_c("tr", {
    staticClass: "table-active"
  }, [_c("th", [_vm._v(_vm._s(_vm.$t("cruds.subjects.fields.title")))]), _vm._v(" "), _c("th", [_vm._v(_vm._s(_vm.$t("cruds.reportManagement.fields.behaviour")) + "(/5)")]), _vm._v(" "), _c("th", [_vm._v(_vm._s(_vm.$t("cruds.reportManagement.fields.effort")) + "(/5)")]), _vm._v(" "), _c("th", [_vm._v(_vm._s(_vm.$t("cruds.reportManagement.fields.classwork")) + "(/40)")]), _vm._v(" "), _c("th", [_vm._v(_vm._s(_vm.$t("cruds.reportManagement.fields.exam")) + "(/50)")]), _vm._v(" "), _c("th", [_vm._v(_vm._s(_vm.$t("cruds.reportManagement.fields.total")))])])]), _vm._v(" "), _c("tbody", _vm._l(_vm.lists.schoolSubjects, function (item, index) {
    return _c("tr", {
      key: index,
      staticStyle: {
        height: "18px"
      }
    }, [_c("td", {
      key: index
    }, [_vm._v("\n                                              " + _vm._s(item.name) + "\n                                          ")]), _vm._v(" "), _c("td", [_c("input", {
      staticClass: "form-control",
      attrs: {
        required: "",
        name: "behaviour",
        type: "number"
      },
      domProps: {
        value: item.behaviour
      },
      on: {
        change: function change($event) {
          return _vm.updateInput($event, index);
        },
        focus: function focus($event) {
          return _vm.focusField("exam_code");
        },
        blur: _vm.clearFocus
      }
    })]), _vm._v(" "), _c("td", [_c("input", {
      staticClass: "form-control",
      attrs: {
        required: "",
        name: "effort",
        type: "number"
      },
      domProps: {
        value: item.effort
      },
      on: {
        change: function change($event) {
          return _vm.updateInput($event, index);
        },
        focus: function focus($event) {
          return _vm.focusField("exam_code");
        },
        blur: _vm.clearFocus
      }
    })]), _vm._v(" "), _c("td", [_c("input", {
      staticClass: "form-control",
      attrs: {
        required: "",
        name: "classwork",
        type: "number"
      },
      domProps: {
        value: item.classwork
      },
      on: {
        change: function change($event) {
          return _vm.updateInput($event, index);
        },
        focus: function focus($event) {
          return _vm.focusField("exam_code");
        },
        blur: _vm.clearFocus
      }
    })]), _vm._v(" "), _c("td", [_c("input", {
      staticClass: "form-control",
      attrs: {
        required: "",
        name: "exam",
        type: "number"
      },
      domProps: {
        value: item.exam
      },
      on: {
        change: function change($event) {
          return _vm.updateInput($event, index);
        },
        focus: function focus($event) {
          return _vm.focusField("exam_code");
        },
        blur: _vm.clearFocus
      }
    })]), _vm._v(" "), _c("td", [_c("input", {
      staticClass: "form-control",
      attrs: {
        required: "",
        readonly: "",
        name: "total",
        type: "number"
      },
      domProps: {
        value: item.total
      },
      on: {
        change: function change($event) {
          return _vm.updateInput($event, index);
        },
        focus: function focus($event) {
          return _vm.focusField("exam_code");
        },
        blur: _vm.clearFocus
      }
    })])]);
  }), 0)])])])]), _vm._v(" "), _c("hr")])])], 1), _vm._v(" "), _c("div", {
    staticClass: "card-footer"
  }, [_c("vue-button-spinner", {
    staticClass: "btn-primary",
    attrs: {
      status: _vm.status,
      isLoading: _vm.loading,
      disabled: _vm.loading
    }
  }, [_vm._v("\n              " + _vm._s(_vm.$t("global.create_new_report")) + "\n            ")])], 1)]), _vm._v(" "), _c("div", {
    staticClass: "card"
  }, [_vm._m(1), _vm._v(" "), _c("div", {
    staticClass: "card-body"
  }), _vm._v(" "), _c("div", {
    staticClass: "card-body"
  }, [_c("bootstrap-alert"), _vm._v(" "), _c("div", {
    staticClass: "row"
  }, [_c("div", {
    staticClass: "col-md-12"
  }, [_c("div", {
    staticClass: "row"
  }, [_c("div", {
    staticClass: "content col-md-12"
  }, [_c("form", {
    on: {
      submit: function submit($event) {
        $event.preventDefault();
      }
    }
  }, [_c("table", {
    staticClass: "table table-hover table-striped w-100"
  }, [_vm._m(2), _vm._v(" "), _c("tbody", _vm._l(_vm.lists.grades, function (item, index) {
    return _c("tr", {
      key: index,
      staticStyle: {
        height: "18px"
      }
    }, [_c("td", {
      key: index
    }, [_vm._v("\n                                              " + _vm._s(item.grade) + "\n                                          ")]), _vm._v(" "), _c("td", [_c("input", {
      staticClass: "form-control",
      attrs: {
        required: "",
        name: "max_marks",
        type: "number"
      },
      domProps: {
        value: item.max_marks
      },
      on: {
        change: function change($event) {
          return _vm.updateRules($event, index);
        },
        focus: function focus($event) {
          return _vm.focusField("exam_code");
        },
        blur: _vm.clearFocus
      }
    })]), _vm._v(" "), _c("td", [_c("input", {
      staticClass: "form-control",
      attrs: {
        required: "",
        name: "minimum_marks",
        type: "number"
      },
      domProps: {
        value: item.minimum_marks
      },
      on: {
        change: function change($event) {
          return _vm.updateRules($event, index);
        },
        focus: function focus($event) {
          return _vm.focusField("exam_code");
        },
        blur: _vm.clearFocus
      }
    })])]);
  }), 0)])])])])])])], 1), _vm._v(" "), _c("div", {
    staticClass: "card-footer"
  }, [_c("vue-button-spinner", {
    staticClass: "btn-primary",
    attrs: {
      status: _vm.status,
      isLoading: _vm.loading,
      disabled: _vm.loading
    }
  }, [_vm._v("\n              " + _vm._s(_vm.$t("global.create_new_report")) + "\n            ")])], 1)])])])])]);
};
var staticRenderFns = [function () {
  var _vm = this,
    _c = _vm._self._c;
  return _c("div", {
    staticClass: "card-icon"
  }, [_c("i", {
    staticClass: "material-icons"
  }, [_vm._v("edit")])]);
}, function () {
  var _vm = this,
    _c = _vm._self._c;
  return _c("div", {
    staticClass: "card-header card-header-primary card-header-icon"
  }, [_c("div", {
    staticClass: "card-icon"
  }, [_c("i", {
    staticClass: "material-icons"
  }, [_vm._v("edit")])]), _vm._v(" "), _c("h4", {
    staticClass: "card-title"
  }, [_vm._v("\n             Config\n              "), _c("strong", [_vm._v("Rules")])])]);
}, function () {
  var _vm = this,
    _c = _vm._self._c;
  return _c("thead", [_c("tr", {
    staticClass: "table-active"
  }, [_c("th", [_vm._v("Grade")]), _vm._v(" "), _c("th", [_vm._v("Max Marks")]), _vm._v(" "), _c("th", [_vm._v("Min Marks")])])]);
}];
render._withStripped = true;


/***/ }),

/***/ "./resources/adminapp/js/cruds/ReportTemplates/Edit.vue":
/*!**************************************************************!*\
  !*** ./resources/adminapp/js/cruds/ReportTemplates/Edit.vue ***!
  \**************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Edit_vue_vue_type_template_id_189e5722___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Edit.vue?vue&type=template&id=189e5722& */ "./resources/adminapp/js/cruds/ReportTemplates/Edit.vue?vue&type=template&id=189e5722&");
/* harmony import */ var _Edit_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Edit.vue?vue&type=script&lang=js& */ "./resources/adminapp/js/cruds/ReportTemplates/Edit.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Edit_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Edit_vue_vue_type_template_id_189e5722___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Edit_vue_vue_type_template_id_189e5722___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/adminapp/js/cruds/ReportTemplates/Edit.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/adminapp/js/cruds/ReportTemplates/Edit.vue?vue&type=script&lang=js&":
/*!***************************************************************************************!*\
  !*** ./resources/adminapp/js/cruds/ReportTemplates/Edit.vue?vue&type=script&lang=js& ***!
  \***************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Edit_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./Edit.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/adminapp/js/cruds/ReportTemplates/Edit.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Edit_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/adminapp/js/cruds/ReportTemplates/Edit.vue?vue&type=template&id=189e5722&":
/*!*********************************************************************************************!*\
  !*** ./resources/adminapp/js/cruds/ReportTemplates/Edit.vue?vue&type=template&id=189e5722& ***!
  \*********************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_loaders_templateLoader_js_ref_6_node_modules_vue_loader_lib_index_js_vue_loader_options_Edit_vue_vue_type_template_id_189e5722___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??ref--6!../../../../../node_modules/vue-loader/lib??vue-loader-options!./Edit.vue?vue&type=template&id=189e5722& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/adminapp/js/cruds/ReportTemplates/Edit.vue?vue&type=template&id=189e5722&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_loaders_templateLoader_js_ref_6_node_modules_vue_loader_lib_index_js_vue_loader_options_Edit_vue_vue_type_template_id_189e5722___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_loaders_templateLoader_js_ref_6_node_modules_vue_loader_lib_index_js_vue_loader_options_Edit_vue_vue_type_template_id_189e5722___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);