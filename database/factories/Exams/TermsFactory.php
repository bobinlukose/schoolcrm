<?php

namespace Database\Factories\Exams;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Terms>
 */
class TermsFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'name' =>$this->faker->randomElement(array('2022-2023','2023-2024')),
            'start_date' => $this->faker->date(),
            'end_date' => $this->faker->date(),
            'sub_branch_id' => 1,
            'branch_id' => 1,
        ];
    }
}
