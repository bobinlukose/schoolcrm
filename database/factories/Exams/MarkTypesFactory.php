<?php

namespace Database\Factories\Exams;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\MarkTypes>
 */
class MarkTypesFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'unique_id' => $this->faker->randomFloat(),
            'title' => $this->faker->randomElement(array('Written','Orals','SEA')),
            'entry_type' => $this->faker->randomLetter(),
            'maximum_mark' => $this->faker->randomDigit(),
            'passing_mark' => $this->faker->randomDigit(),
            'order' => $this->faker->randomDigit(),
            'status' => $this->faker->randomDigit(),
            'branch_id' => 1,
        ];
    }
}
