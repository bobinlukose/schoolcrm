<?php

namespace Database\Factories\Common;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Contact>
 */
class ContactFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'type' => $this->faker->sentence(),
            'first_name' => $this->faker->firstName(),
            'last_name' => $this->faker->lastName(),
            'home_phone' => $this->faker->phoneNumber(),
            'work_phone' => $this->faker->phoneNumber(),
            'mobile' => $this->faker->phoneNumber(),
            'fax' => $this->faker->postcode(),
            'email' => $this->faker->email(),
            'branch_id' =>null,
        ];
    }
}
