<?php

namespace Database\Factories\Classes;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Sections>
 */
class SectionsFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'name' =>$this->faker->randomElement(array('Session 1','Session 2','Session 3','Session 4')),
            'unique_id' => $this->faker->randomFloat(),
            'is_group' => false,
            'branch_id' => 1,
        ];
    }
}
