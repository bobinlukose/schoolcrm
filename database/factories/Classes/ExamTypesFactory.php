<?php

namespace Database\Factories\Classes;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\ExamTypes>
 */
class ExamTypesFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'unique_id' => $this->faker->randomFloat(),
            'title' => $this->faker->randomElement(array('Restricted exams','Closed exams',)),
            'description' => $this->faker->randomLetter(),
            'branch_id' => 1,
        ];
    }
}
