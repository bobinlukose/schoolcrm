<?php

namespace Database\Factories\Classes;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Classes>
 */
class ClassesFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'name' =>$this->faker->randomElement(array('Class 1','Class 2','Class 3','Class 4')),
            'unique_id' => $this->faker->randomFloat(),
            'branch_id' => 1,
        ];
    }
}
