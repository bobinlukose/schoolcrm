<?php

namespace Database\Factories\Classes;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Subjects>
 */
class SubjectsFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'unique_id' => $this->faker->randomFloat(),
            'name' => $this->faker->randomElement(array('ENGLISH','MATHS','SOCIAL','SCIENCE','IT')),
            'branch_id' => 1,
            'class_id' => 1,
            'is_published' => 1,
            'is_group' => false,
        ];
    }
}
