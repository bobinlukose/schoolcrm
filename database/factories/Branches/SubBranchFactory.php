<?php

namespace Database\Factories\Branches;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\SubBranch>
 */
class SubBranchFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'name' =>$this->faker->randomElement(array('Sub Branch')),
            'unique_id' => $this->faker->randomFloat(),
            'parent_id' => 1,
            'parent_type' => null,
            'hierarchy_level' => 0,
            'hierarchy_path' => null,
        ];
    }
}
