<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    public function run()
    {
        $this->call([
            PermissionsTableSeeder::class,
            RolesTableSeeder::class,
            PermissionRoleTableSeeder::class,
            UsersTableSeeder::class,
            RoleUserTableSeeder::class,
            AddressesTableSeeder::class,
            ContactsTableSeeder::class,
            BranchTableSeeder::class,
            ExamTypesSeeder::class,
            SectionsSeeder::class,
            ClassesSeeder::class,
            SubBranchSeeder::class,
            TermsSeeder::class,
            SubjectsSeeder::class,
            MArkTypesSeeder::class,
        ]);
    }
}
