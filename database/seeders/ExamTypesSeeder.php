<?php

namespace Database\Seeders;

use App\Models\ExamTypes;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ExamTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        \App\Models\Classes\ExamTypes::factory(2)->create();
    }
}
