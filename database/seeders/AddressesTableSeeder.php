<?php

namespace Database\Seeders;

use App\Models\Common\Addresses;
use Faker\Provider\Address;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class AddressesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        \App\Models\Common\Addresses::factory(2)->create();
    }
}
