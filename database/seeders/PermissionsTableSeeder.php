<?php

namespace Database\Seeders;

use App\Models\Permission;
use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{
    public function run()
    {
        $permissions = [
            [
                'id'    => 1,
                'title' => 'user_management_access',
            ],
            [
                'id'    => 2,
                'title' => 'permission_create',
            ],
            [
                'id'    => 3,
                'title' => 'permission_edit',
            ],
            [
                'id'    => 4,
                'title' => 'permission_show',
            ],
            [
                'id'    => 5,
                'title' => 'permission_delete',
            ],
            [
                'id'    => 6,
                'title' => 'permission_access',
            ],
            [
                'id'    => 7,
                'title' => 'role_create',
            ],
            [
                'id'    => 8,
                'title' => 'role_edit',
            ],
            [
                'id'    => 9,
                'title' => 'role_show',
            ],
            [
                'id'    => 10,
                'title' => 'role_delete',
            ],
            [
                'id'    => 11,
                'title' => 'role_access',
            ],
            [
                'id'    => 12,
                'title' => 'user_create',
            ],
            [
                'id'    => 13,
                'title' => 'user_edit',
            ],
            [
                'id'    => 14,
                'title' => 'user_show',
            ],
            [
                'id'    => 15,
                'title' => 'user_delete',
            ],
            [
                'id'    => 16,
                'title' => 'user_access',
            ],
            [
                'id'    => 17,
                'title' => 'product_management_access',
            ],
            [
                'id'    => 18,
                'title' => 'product_category_create',
            ],
            [
                'id'    => 19,
                'title' => 'product_category_edit',
            ],
            [
                'id'    => 20,
                'title' => 'product_category_show',
            ],
            [
                'id'    => 21,
                'title' => 'product_category_delete',
            ],
            [
                'id'    => 22,
                'title' => 'product_category_access',
            ],
            [
                'id'    => 23,
                'title' => 'product_tag_create',
            ],
            [
                'id'    => 24,
                'title' => 'product_tag_edit',
            ],
            [
                'id'    => 25,
                'title' => 'product_tag_show',
            ],
            [
                'id'    => 26,
                'title' => 'product_tag_delete',
            ],
            [
                'id'    => 27,
                'title' => 'product_tag_access',
            ],
            [
                'id'    => 28,
                'title' => 'product_create',
            ],
            [
                'id'    => 29,
                'title' => 'product_edit',
            ],
            [
                'id'    => 30,
                'title' => 'product_show',
            ],
            [
                'id'    => 31,
                'title' => 'product_delete',
            ],
            [
                'id'    => 32,
                'title' => 'product_access',
            ],
            [
                'id'    => 33,
                'title' => 'faq_management_access',
            ],
            [
                'id'    => 34,
                'title' => 'faq_category_create',
            ],
            [
                'id'    => 35,
                'title' => 'faq_category_edit',
            ],
            [
                'id'    => 36,
                'title' => 'faq_category_show',
            ],
            [
                'id'    => 37,
                'title' => 'faq_category_delete',
            ],
            [
                'id'    => 38,
                'title' => 'faq_category_access',
            ],
            [
                'id'    => 39,
                'title' => 'faq_question_create',
            ],
            [
                'id'    => 40,
                'title' => 'faq_question_edit',
            ],
            [
                'id'    => 41,
                'title' => 'faq_question_show',
            ],
            [
                'id'    => 42,
                'title' => 'faq_question_delete',
            ],
            [
                'id'    => 43,
                'title' => 'faq_question_access',
            ],
            [
                'id'    => 44,
                'title' => 'contact_management_access',
            ],
            [
                'id'    => 45,
                'title' => 'management_create',
            ],
            [
                'id'    => 46,
                'title' => 'management_edit',
            ],
            [
                'id'    => 47,
                'title' => 'management_show',
            ],
            [
                'id'    => 48,
                'title' => 'management_delete',
            ],
            [
                'id'    => 49,
                'title' => 'management_access',
            ],
            [
                'id'    => 50,
                'title' => 'branch_create',
            ],
            [
                'id'    => 51,
                'title' => 'branch_edit',
            ],
            [
                'id'    => 52,
                'title' => 'branch_show',
            ],
            [
                'id'    => 53,
                'title' => 'branch_delete',
            ],
            [
                'id'    => 54,
                'title' => 'branch_access',
            ],
            [
                'id'    => 55,
                'title' => 'content_management_access',
            ],
            [
                'id'    => 56,
                'title' => 'content_category_create',
            ],
            [
                'id'    => 57,
                'title' => 'content_category_edit',
            ],
            [
                'id'    => 58,
                'title' => 'content_category_show',
            ],
            [
                'id'    => 59,
                'title' => 'content_category_delete',
            ],
            [
                'id'    => 60,
                'title' => 'content_category_access',
            ],
            [
                'id'    => 61,
                'title' => 'content_tag_create',
            ],
            [
                'id'    => 62,
                'title' => 'content_tag_edit',
            ],
            [
                'id'    => 63,
                'title' => 'content_tag_show',
            ],
            [
                'id'    => 64,
                'title' => 'content_tag_delete',
            ],
            [
                'id'    => 65,
                'title' => 'content_tag_access',
            ],
            [
                'id'    => 66,
                'title' => 'content_page_create',
            ],
            [
                'id'    => 67,
                'title' => 'content_page_edit',
            ],
            [
                'id'    => 68,
                'title' => 'content_page_show',
            ],
            [
                'id'    => 69,
                'title' => 'content_page_delete',
            ],
            [
                'id'    => 70,
                'title' => 'content_page_access',
            ],
            [
                'id'    => 71,
                'title' => 'basic_c_r_m_access',
            ],
            [
                'id'    => 72,
                'title' => 'crm_status_create',
            ],
            [
                'id'    => 73,
                'title' => 'crm_status_edit',
            ],
            [
                'id'    => 74,
                'title' => 'crm_status_show',
            ],
            [
                'id'    => 75,
                'title' => 'crm_status_delete',
            ],
            [
                'id'    => 76,
                'title' => 'crm_status_access',
            ],
            [
                'id'    => 77,
                'title' => 'crm_customer_create',
            ],
            [
                'id'    => 78,
                'title' => 'crm_customer_edit',
            ],
            [
                'id'    => 79,
                'title' => 'crm_customer_show',
            ],
            [
                'id'    => 80,
                'title' => 'crm_customer_delete',
            ],
            [
                'id'    => 81,
                'title' => 'crm_customer_access',
            ],
            [
                'id'    => 82,
                'title' => 'crm_note_create',
            ],
            [
                'id'    => 83,
                'title' => 'crm_note_edit',
            ],
            [
                'id'    => 84,
                'title' => 'crm_note_show',
            ],
            [
                'id'    => 85,
                'title' => 'crm_note_delete',
            ],
            [
                'id'    => 86,
                'title' => 'crm_note_access',
            ],
            [
                'id'    => 87,
                'title' => 'crm_document_create',
            ],
            [
                'id'    => 88,
                'title' => 'crm_document_edit',
            ],
            [
                'id'    => 89,
                'title' => 'crm_document_show',
            ],
            [
                'id'    => 90,
                'title' => 'crm_document_delete',
            ],
            [
                'id'    => 91,
                'title' => 'crm_document_access',
            ],
            [
                'id'    => 92,
                'title' => 'subjects_access',
            ],
            [
                'id'    => 93,
                'title' => 'subjects_create',
            ],
            [
                'id'    => 94,
                'title' => 'subjects_edit',
            ],
            [
                'id'    => 95,
                'title' => 'subjects_show',
            ],
            [
                'id'    => 96,
                'title' => 'subjects_delete',
            ],
            [
                'id'    => 97,
                'title' => 'subjects_access',
            ],
            [
                'id'    => 98,
                'title' => 'income_category_create',
            ],
            [
                'id'    => 99,
                'title' => 'income_category_edit',
            ],
            [
                'id'    => 100,
                'title' => 'income_category_show',
            ],
            [
                'id'    => 101,
                'title' => 'income_category_delete',
            ],
            [
                'id'    => 102,
                'title' => 'income_category_access',
            ],
            [
                'id'    => 103,
                'title' => 'expense_create',
            ],
            [
                'id'    => 104,
                'title' => 'expense_edit',
            ],
            [
                'id'    => 105,
                'title' => 'expense_show',
            ],
            [
                'id'    => 106,
                'title' => 'expense_delete',
            ],
            [
                'id'    => 107,
                'title' => 'expense_access',
            ],
            [
                'id'    => 108,
                'title' => 'income_create',
            ],
            [
                'id'    => 109,
                'title' => 'income_edit',
            ],
            [
                'id'    => 110,
                'title' => 'income_show',
            ],
            [
                'id'    => 111,
                'title' => 'income_delete',
            ],
            [
                'id'    => 112,
                'title' => 'income_access',
            ],
            [
                'id'    => 113,
                'title' => 'expense_report_create',
            ],
            [
                'id'    => 114,
                'title' => 'expense_report_edit',
            ],
            [
                'id'    => 115,
                'title' => 'expense_report_show',
            ],
            [
                'id'    => 116,
                'title' => 'expense_report_delete',
            ],
            [
                'id'    => 117,
                'title' => 'expense_report_access',
            ],
            [
                'id'    => 118,
                'title' => 'subject_create',
            ],
            [
                'id'    => 119,
                'title' => 'subject_edit',
            ],
            [
                'id'    => 120,
                'title' => 'subject_show',
            ],
            [
                'id'    => 121,
                'title' => 'subject_delete',
            ],
            [
                'id'    => 122,
                'title' => 'subject_access',
            ],
            [
                'id'    => 123,
                'title' => 'lesson_create',
            ],
            [
                'id'    => 124,
                'title' => 'lesson_edit',
            ],
            [
                'id'    => 125,
                'title' => 'lesson_show',
            ],
            [
                'id'    => 126,
                'title' => 'lesson_delete',
            ],
            [
                'id'    => 127,
                'title' => 'lesson_access',
            ],
            [
                'id'    => 128,
                'title' => 'test_create',
            ],
            [
                'id'    => 129,
                'title' => 'test_edit',
            ],
            [
                'id'    => 130,
                'title' => 'test_show',
            ],
            [
                'id'    => 131,
                'title' => 'test_delete',
            ],
            [
                'id'    => 132,
                'title' => 'test_access',
            ],
            [
                'id'    => 133,
                'title' => 'question_create',
            ],
            [
                'id'    => 134,
                'title' => 'question_edit',
            ],
            [
                'id'    => 135,
                'title' => 'question_show',
            ],
            [
                'id'    => 136,
                'title' => 'question_delete',
            ],
            [
                'id'    => 137,
                'title' => 'question_access',
            ],
            [
                'id'    => 138,
                'title' => 'question_option_create',
            ],
            [
                'id'    => 139,
                'title' => 'question_option_edit',
            ],
            [
                'id'    => 140,
                'title' => 'question_option_show',
            ],
            [
                'id'    => 141,
                'title' => 'question_option_delete',
            ],
            [
                'id'    => 142,
                'title' => 'question_option_access',
            ],
            [
                'id'    => 143,
                'title' => 'test_result_create',
            ],
            [
                'id'    => 144,
                'title' => 'test_result_edit',
            ],
            [
                'id'    => 145,
                'title' => 'test_result_show',
            ],
            [
                'id'    => 146,
                'title' => 'test_result_delete',
            ],
            [
                'id'    => 147,
                'title' => 'test_result_access',
            ],
            [
                'id'    => 148,
                'title' => 'test_answer_create',
            ],
            [
                'id'    => 149,
                'title' => 'test_answer_edit',
            ],
            [
                'id'    => 150,
                'title' => 'test_answer_show',
            ],
            [
                'id'    => 151,
                'title' => 'test_answer_delete',
            ],
            [
                'id'    => 152,
                'title' => 'test_answer_access',
            ],
            [
                'id'    => 153,
                'title' => 'exam_management_access',
            ],
            [
                'id'    => 154,
                'title' => 'exam_create',
            ],
            [
                'id'    => 155,
                'title' => 'exam_edit',
            ],
            [
                'id'    => 156,
                'title' => 'exam_show',
            ],
            [
                'id'    => 157,
                'title' => 'exam_delete',
            ],
            [
                'id'    => 158,
                'title' => 'exam_access',
            ],
            [
                'id'    => 159,
                'title' => 'school_create',
            ],
            [
                'id'    => 160,
                'title' => 'school_edit',
            ],
            [
                'id'    => 161,
                'title' => 'school_show',
            ],
            [
                'id'    => 162,
                'title' => 'school_delete',
            ],
            [
                'id'    => 163,
                'title' => 'school_access',
            ],
            [
                'id'    => 164,
                'title' => 'classes_create',
            ],
            [
                'id'    => 165,
                'title' => 'classes_edit',
            ],
            [
                'id'    => 166,
                'title' => 'classes_show',
            ],
            [
                'id'    => 167,
                'title' => 'classes_delete',
            ],
            [
                'id'    => 168,
                'title' => 'classes_access',
            ],
            [
                'id'    => 169,
                'title' => 'section_create',
            ],
            [
                'id'    => 170,
                'title' => 'section_edit',
            ],
            [
                'id'    => 171,
                'title' => 'section_show',
            ],
            [
                'id'    => 172,
                'title' => 'section_delete',
            ],
            [
                'id'    => 173,
                'title' => 'section_access',
            ],
            [
                'id'    => 174,
                'title' => 'mark_type_create',
            ],
            [
                'id'    => 175,
                'title' => 'mark_type_edit',
            ],
            [
                'id'    => 176,
                'title' => 'mark_type_show',
            ],
            [
                'id'    => 177,
                'title' => 'mark_type_delete',
            ],
            [
                'id'    => 178,
                'title' => 'mark_type_access',
            ],
            [
                'id'    => 179,
                'title' => 'report_management_access',
            ],
            [
                'id'    => 180,
                'title' => 'report_access',
            ],
            [
                'id'    => 181,
                'title' => 'report_create',
            ],
            [
                'id'    => 182,
                'title' => 'report_edit',
            ],
            [
                'id'    => 183,
                'title' => 'report_show',
            ],
            [
                'id'    => 184,
                'title' => 'report_delete',
            ],
            [
                'id'    => 185,
                'title' => 'students_management_access',
            ],
            [
                'id'    => 186,
                'title' => 'students_create',
            ],
            [
                'id'    => 187,
                'title' => 'students_edit',
            ],
            [
                'id'    => 188,
                'title' => 'students_show',
            ],
            [
                'id'    => 189,
                'title' => 'students_delete',
            ],
            [
                'id'    => 190,
                'title' => 'students_access',
            ],

        ];

        Permission::insert($permissions);
    }
}
