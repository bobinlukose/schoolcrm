<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

use Illuminate\Database\Eloquent\SoftDeletes;

return new class extends Migration
{
    use SoftDeletes;
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('mark_types', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('branch_id');
            $table->string('unique_id')->unique();
            $table->string('title');
            $table->string('entry_type');
            $table->integer('maximum_mark');
            $table->integer('passing_mark');
            $table->integer('order');
            $table->boolean('is_published')->default(0)->nullable();
            $table->string('status');
            $table->timestamps();
            $table->softDeletes();

            // Foreign key constraint
            $table->foreign('branch_id')->references('id')->on('branches')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('mark_types');
    }
};
