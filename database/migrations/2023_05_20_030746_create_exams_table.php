<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

use Illuminate\Database\Eloquent\SoftDeletes;

return new class extends Migration
{
    use SoftDeletes;
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('exams', function (Blueprint $table) {
            $table->id();
            $table->string('unique_id')->unique();
            $table->string('exam_title');
            $table->unsignedBigInteger('branch_id');
            $table->date('start_date');
            $table->date('end_date');
            $table->unsignedBigInteger('type_id');
            $table->string('code');
            $table->unsignedBigInteger('class_id');
            $table->string('lesson');
            $table->unsignedBigInteger('section_id');
            $table->string('term');
            $table->unsignedBigInteger('mark_type_id');
            $table->string('status');
            $table->text('comments')->nullable();
            $table->unsignedBigInteger('subject_id');
            $table->string('topic');
            $table->timestamps();
            $table->softDeletes();

            // Foreign key constraints
            $table->foreign('branch_id')->references('id')->on('branches')->onDelete('cascade');
            $table->foreign('class_id')->references('id')->on('classes')->onDelete('cascade');
            $table->foreign('section_id')->references('id')->on('sections')->onDelete('cascade');
            $table->foreign('mark_type_id')->references('id')->on('mark_types')->onDelete('cascade');
            $table->foreign('type_id')->references('id')->on('exam_types')->onDelete('cascade');
            $table->foreign('subject_id')->references('id')->on('subjects')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('exams');
    }
};
