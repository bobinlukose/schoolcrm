<?php
use App\Http\Controllers\Api\V1\Admin\UsersApiController;

Route::post('login',[UsersApiController::class,'loginUser']);


Route::group(['prefix' => 'v1', 'as' => 'api.', 'namespace' => 'Api\V1\Admin', 'middleware' => ['auth:sanctum']], function () {
    Route::get('logout',[UsersApiController::class,'logout']);
    // Abilities
    Route::apiResource('abilities', 'AbilitiesController', ['only' => ['index']]);

    // Locales
    Route::get('locales/languages', 'LocalesController@languages')->name('locales.languages');
    Route::get('locales/messages', 'LocalesController@messages')->name('locales.messages');

    // Dashboard
    Route::get('dashboard', 'DashboardApiController@index')->name('dashboard');

    // Permissions
    Route::resource('permissions', 'PermissionsApiController');

    // Roles
    Route::resource('roles', 'RolesApiController');

    // Users
    Route::resource('users', 'UsersApiController');

    // Product Category
    Route::post('product-categories/media', 'ProductCategoryApiController@storeMedia')->name('product-categories.storeMedia');
    Route::resource('product-categories', 'ProductCategoryApiController');

    // Product Tag
    Route::resource('product-tags', 'ProductTagApiController');

    // Product
    Route::post('products/media', 'ProductApiController@storeMedia')->name('products.storeMedia');
    Route::resource('products', 'ProductApiController');

    // Faq Category
    Route::resource('faq-categories', 'FaqCategoryApiController');

    // Faq Question
    Route::resource('faq-questions', 'FaqQuestionApiController');

    // Contact Company
    Route::resource('management', 'Management\ManagementApiController');
    Route::post('management/media', 'Management\ManagementApiController@storeMedia')->name('contact-companies.storeMedia');

    // Students Details
    Route::resource('students', 'Students\StudentsApiController');
    Route::post('students/media', 'Students\StudentsApiController@storeMedia')->name('students.storeMedia');

    // Contact Contacts
    Route::resource('branch-details', 'Branch\BranchApiController');

    // Content Category
    Route::resource('content-categories', 'ContentCategoryApiController');

    // Content Tag
    Route::resource('content-tags', 'ContentTagApiController');

    // Content Page
    Route::post('content-pages/media', 'ContentPageApiController@storeMedia')->name('content-pages.storeMedia');
    Route::resource('content-pages', 'ContentPageApiController');

    // Crm Status
    Route::resource('crm-statuses', 'CrmStatusApiController');

    // Report Generation
    Route::resource('reports', 'Reports\ReportsApiController');

    Route::resource('template', 'Reports\TemplateApiController');
    Route::post('generate-report-card', 'Reports\TemplateApiController@generateReport')->name('generate-report-card.generateReport');

    // Crm Note
    Route::resource('crm-notes', 'CrmNoteApiController');

    // Crm Document
    Route::post('crm-documents/media', 'CrmDocumentApiController@storeMedia')->name('crm-documents.storeMedia');
    Route::resource('crm-documents', 'CrmDocumentApiController');

    // Expense Category
   // Route::resource('expense-categories', 'Classes\SubjectsApiController');

    // Income Category
    Route::resource('income-categories', 'IncomeCategoryApiController');

    // Expense
    Route::resource('expenses', 'ExpenseApiController');

    // Income
    Route::resource('incomes', 'IncomeApiController');

    // Expense Report
    Route::apiResource('expense-reports', 'ExpenseReportApiController', ['only' => ['index']]);

    // Subjects
    Route::post('subjects/media', 'Classes\SubjectsApiController@storeMedia')->name('subjects.storeMedia');
    Route::resource('subject', 'Classes\SubjectsApiController');


    //Classes
    Route::resource('class', 'Classes\ClassesApiController');

    // Mark Type
    Route::resource('mark-type', 'School\MarkTypeApiController');

    // Lessons
    Route::post('lessons/media', 'Classes\LessonsApiController@storeMedia')->name('lessons.storeMedia');
    Route::resource('lessons', 'Classes\LessonsApiController');

    // Tests
    Route::resource('tests', 'Classes\ExamTopicApiController');

    // Sections
    Route::resource('sections', 'Classes\SectionApiController');

    // Questions
    Route::post('questions/media', 'QuestionsApiController@storeMedia')->name('questions.storeMedia');
    Route::resource('questions', 'QuestionsApiController');

    // Question Options
    Route::resource('question-options', 'QuestionOptionsApiController');

    // Topic Results
    Route::resource('test-results', 'TestResultsApiController');

    // Topic Answers
    Route::resource('test-answers', 'TestAnswersApiController');

    // Users
    Route::resource('exam', 'ExamApiController');
});
